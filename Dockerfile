FROM python:latest

WORKDIR /app

ADD scripts/requirements.txt .
RUN pip install -r requirements.txt

COPY . .

CMD python scripts/yaml_to_html.py && \
  cp -R /app/examples ./html/. && \
  cp -R /app/assets ./html/. && \
  cp -R /app/json-schema ./html/. && \
  sed -ri "s|DOMAIN|www-staging.hestia.earth|g" ./html/examples/*.jsonld
