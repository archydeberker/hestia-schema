#!/bin/sh
rm -rf html
docker build --progress=plain \
  -t hestia-schema:test \
  .
docker run --rm -v ${PWD}:/app hestia-schema:test
mv index.html html/.
