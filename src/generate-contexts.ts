import { writeFileSync } from 'fs';
import { join } from 'path';

import { ensureDir, listYmlDir, readYmlFile, dependencies, IProperty, PropertyType } from './generate-typescript';

const CONTEXT_DIR = join(__dirname, '..', 'context');
const CONTEXT_VERSION = 1.1; // @see https://json-ld.org/spec/latest/json-ld/
const GEOJSON_CONTEXT_URL = 'https://tools.ietf.org/html/rfc7946';
const [ DOMAIN ] = process.argv.slice(2);
const maxRecursion = 3;
const domain = DOMAIN || process.env.DOMAIN || 'www.hestia.earth';

ensureDir(CONTEXT_DIR);

const generateLinkedContext = (name: string, level = 1) => {
  const content = readYmlFile(`${name}.yaml`);
  const generateDependencies = level < maxRecursion;
  const contexts = generateDependencies ? dependencies(content) : [];
  return {
    ...contexts.reduce((prev, curr) => ({ ...prev, ...generateLinkedContext(curr, level + 1) }), {}),
    [name]: {
      '@id': `https://${domain}/schema/${name}`,
      '@context': {
        '@propagate': false,
        '@vocab': `https://${domain}/schema/${name}#`,
        ...(generateDependencies ? generateProperties(name, content.properties) : {})
      }
    }
  };
};

const generateProperties = (name: string, properties: IProperty[]) => ({
  ...properties.filter(prop => prop.type === PropertyType.array).reduce((prev, curr) => ({
    ...prev, [curr.name]: {
      '@container': '@list'
    }
  }), {}),
  ...properties.filter(prop => prop.type === PropertyType.GeoJSON).reduce((prev, curr) => ({
    ...prev, [curr.name]: {
      '@id': `https://${domain}/schema/${name}#${curr.name}`,
      '@context': {
        type: GEOJSON_CONTEXT_URL,
        features: `${GEOJSON_CONTEXT_URL}#section-3.2`,
        geometry: `${GEOJSON_CONTEXT_URL}#section-3.1`,
        properties: GEOJSON_CONTEXT_URL,
        coordinates: {
          '@id': `${GEOJSON_CONTEXT_URL}#section-3.1.3`,
          '@container': '@list'
        },
        'bbox': {
          '@id': `${GEOJSON_CONTEXT_URL}#section-5`,
          '@container': '@list'
        }
      }
    }
  }), {}),
  ...properties.filter(prop => prop.type === PropertyType.iri).reduce((prev, curr) => ({
    ...prev, [curr.name]: {
      '@id': `https://${domain}/schema/${name}#${curr.name}`,
      '@type': '@id'
    }
  }), {})
});

const generateContext = ({ content, name }) => {
  console.log(`Processing file: ${name}.yaml`);
  const contexts = dependencies(content).reduce((prev, curr) => ({ ...prev, ...generateLinkedContext(curr) }), {});
  const context = {
    '@context': {
      '@version': CONTEXT_VERSION,
      '@base': `https://${domain}/${name.toLowerCase()}/`,
      '@vocab': `https://${domain}/schema/${name}#`,
      ...Object.keys(contexts).filter(key => key !== name).reduce((prev, key) => ({ ...prev, [key]: contexts[key] }), {}),
      ...generateProperties(name, content.properties)
    }
  };
  writeFileSync(join(CONTEXT_DIR, `${name}.jsonld`), JSON.stringify(context, null, 2));
};

const run = () => {
  const files = listYmlDir();
  const data = files.map(file => ({ content: readYmlFile(file), name: file.replace('.yaml', '') }));
  data.filter(({ content }) => content.type === 'Node').map(generateContext);
};

run();
