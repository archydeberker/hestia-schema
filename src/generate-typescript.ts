import { readdirSync, readFileSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';
import { parse } from 'yamljs';

const version = require(join(__dirname, '..', 'package.json')).version;
const defaultUploadLimit = 1000;
const newLineJoin = ',\n  ';

export enum PropertyType {
  array = 'array',
  number = 'number',
  integer = 'integer',
  string = 'string',
  boolean = 'boolean',
  date = 'date',
  'date-time' = 'date-time',
  GeoJSON = 'GeoJSON',
  iri = 'iri'
}

const typeToClass: {
  [type in PropertyType]: string;
} = {
  [PropertyType.array]: 'any[]',
  [PropertyType.number]: 'number',
  [PropertyType.integer]: 'number',
  [PropertyType.string]: 'string',
  [PropertyType.boolean]: 'boolean',
  [PropertyType.date]: 'Date',
  [PropertyType['date-time']]: 'Date',
  [PropertyType.GeoJSON]: 'any',
  [PropertyType.iri]: 'any'
};

export interface IProperty {
  name: string;
  type: PropertyType;
  doc?: string;
  enum?: string[];
  const?: any;
  pattern?: string;
  // default value if none is provided
  default?: any;
  unique?: boolean;
  required?: boolean;
  searchable?: boolean;
};

const defaultProperties: IProperty[] = [{
  name: 'type',
  type: PropertyType.string
}, {
  name: '@type',
  type: PropertyType.string
}, {
  name: '@id',
  type: PropertyType.string
}, {
  name: 'id',
  type: PropertyType.string
}];

export interface IClass {
  name: string;
  type: 'Node' | 'Blank Node';
  examples?: string[];
  doc: string;
  properties: IProperty[];
  uploadLimit?: number;
  validation?: any;
}

interface ISortedProperties {
  [key: string]: {
    order: string;
    type: string;
  };
}

interface IGeneratedClass {
  name: string;
  isNode: boolean;
  searchable: string[];
  sortedProperties: ISortedProperties;
  uploadLimit: number;
}

export const findPropertyType = (type: string) => type in typeToClass ? typeToClass[type] : type;

export const cleanType = (type: string) => type
  .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(List\[)([a-zA-Z]*)(\])/g, '$2[]')
  .replace(/(array\[)([a-zA-Z]*)(\])/g, '$2[]');

export const YAML_DIR = join(__dirname, '..', 'yaml');

export const ensureDir = (dir: string) => {
  try { mkdirSync(dir, { recursive: true }) } catch (err) { }
};

export const readYmlFile = (file: string) => parse(readFileSync(join(YAML_DIR, file), 'utf8')).class as IClass;

export const listYmlDir = () => readdirSync(YAML_DIR);

export const dependencies = ({ name, properties }: IClass) => properties
  .map(({ type }) => findPropertyType(type))
  .filter(type => type.includes('Embed') || type.includes('Ref') || type.includes('List'))
  .map(type => cleanType(type).replace(/\[\]/g, ''))
  .filter(type => !(type in typeToClass) && type !== name);

const CLASS_DIR = join(__dirname, '@hestia-earth', 'schema');

ensureDir(CLASS_DIR);

const cleanPropertyName = (name: string) =>
  name.includes('-') || name.includes(' ') ? `'${name}'` : name;

const variableName = (...parts: string[]) =>
  parts
    .flat()
    .filter(Boolean)
    .map((part, index) => index === 0 ?
      `${part.charAt(0).toLowerCase()}${part.substring(1)}` :
      `${part.charAt(0).toUpperCase()}${part.substring(1)}`
    )
    .join('');

const enumName = (...parts: string[]) =>
  parts.flat().filter(Boolean).map(part => `${part.charAt(0).toUpperCase()}${part.substring(1)}`).join('');

const propertyIsEnum = (property: Partial<IProperty>) =>
  !!property.enum && property.enum.every(val => typeof val === 'string');

const propertyEnumName = (className: string, { name }: Partial<IProperty>) => enumName(className, name);

const propertyEnum = (className: string, property: Partial<IProperty>) => propertyIsEnum(property) ?
  propertyEnumName(className, property) :
  property.enum.map(t => property.type === PropertyType.string ? `'${t}'` : t).join('|');

const propertyType = (prop: IProperty, className?: string) =>
  prop.enum && className ? propertyEnum(className, prop) : cleanType(findPropertyType(prop.type));

const generateProperty = (className: string, isClass = true) => ({ doc, const: constValue, ...prop }: IProperty) =>
  `${doc ? `/**
  * ${doc.trim()}
  */` : ''}
  ${cleanPropertyName(prop.name)}?: ${propertyType(prop, className)}${isClass && constValue ? ` = ${constValue}` : ''};
`;

const JSONContent = ({ className, doc, properties }) => `
/**
 * ${doc.trim()}
 */
export class ${className} extends JSON<SchemaType.${className}> {
  ${properties.map(generateProperty(className)).join('\n  ')}
}`;

const JSONLDContent = ({ className, doc, properties }) => `
/**
 * ${doc.trim()}
 */
export interface I${className}JSONLD extends JSONLD<NodeType.${className}> {
  ${properties.filter(({ name }) => name !== 'name').map(generateProperty(className, false)).join('\n  ')}
}`;

const generateEnums = (className: string, properties: IProperty[]) =>
  properties.filter(propertyIsEnum).map(property => `
export enum ${propertyEnumName(className, property)} {
  ${property.enum.sort().map(val => `${cleanPropertyName(val)} = '${val}'`).join(newLineJoin)}
}`).join('\n\n');

const getValidations = (className: string, validation: any) =>
  (validation.allOf || [])
    .filter(v => v.if.required && v.if.required.length === 1 && v.then.properties)
    .map(v => ({
      name: v.if.required[0],
      property: v.then.properties[v.if.required[0]]
    }))
    .filter(({ property }) => !!property)
    .map(({ name, property }) => ({
      name,
      properties: 'contains' in property || 'items' in property ?
        (property.contains || property.items).properties : property.properties
    }))
    .flatMap(({ name, properties }) =>
      Object.keys(properties)
        .filter(propName => 'enum' in properties[propName])
        .map(propName => ({
          variable: variableName(className, name, propName),
          enum: enumName(propName),
          values: properties[propName].enum.map(value => `${enumName(propName)}.${value}`)
        }))
    );

const generateValidations = (className: string, validations) =>
  validations.map(({ variable, values }) => `
export const ${variable} = [
  ${values.map(value => `${className}${value}`).join(newLineJoin)}
]`).join('\n\n');

const sortedProperties = (properties: IProperty[]): ISortedProperties =>
  properties.reduce((prev, prop, index) => ({
    ...prev,
    [prop.name]: {
      order: index.toString().padStart(properties.length.toString().length, '0'),
      type: propertyType(prop).replace(/\[\]/g, '')
    }
  }), {});

const generateClass = (file: string): IGeneratedClass => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const properties: IProperty[] = data.properties;
  const importedClasses = dependencies(data);
  // TODO: should handle any className, not just Term
  const validationClass = 'Term';
  const validations = data.validation ? getValidations(data.name, data.validation) : [];
  const importedEnums = validations.map(val => val.enum);
  const hasJSONLD = data.type === 'Node';
  const content = `// auto-generated content
  import { JSON, ${hasJSONLD ? 'JSONLD, NodeType, ' : ''}SchemaType } from './types';
${[...new Set(importedClasses)].map(name => `import { ${name} } from './${name}';`).join('\n')}
${[...new Set(importedEnums)].map(name => `import { ${validationClass}${name} } from './${validationClass}';`).join('\n')}
${generateEnums(data.name, properties)}
${generateValidations(validationClass, validations)}
${JSONContent({ className: data.name, doc: data.doc, properties })}
${hasJSONLD ? JSONLDContent({ className: data.name, doc: data.doc, properties }) : ''}`;
  writeFileSync(join(CLASS_DIR, `${data.name}.ts`), content);
  return {
    name: data.name,
    isNode: hasJSONLD,
    searchable: properties.filter(prop => prop.searchable).map(prop => prop.name),
    sortedProperties: sortedProperties([...defaultProperties, ...properties]),
    uploadLimit: data.uploadLimit || defaultUploadLimit
  };
};

const generateTypes = (classes: IGeneratedClass[]) => {
  const content = `// auto-generated content

export enum NodeType {
  ${classes.filter(({ isNode }) => isNode).map(({ name }) => `${name} = '${name}'`).join(newLineJoin)}
}

export enum SchemaType {
  ${classes.map(({ name }) => `${name} = '${name}'`).join(newLineJoin)}
}

export const searchableProperties: {
  [type in SchemaType]: string[];
} = {
  ${classes.map(({ name, searchable }) => `${name}: [${searchable.map(value => `'${value}'`).join(', ')}]`).join(newLineJoin)}
};

export enum UploadLimit {
  ${classes.filter(({ isNode }) => isNode).map(({ name, uploadLimit }) => `${name} = ${uploadLimit}`).join(newLineJoin)}
}

export class JSON<T extends SchemaType> {
  type: T;
  /**
   * Not required, used to generate contextualized unique id.
   */
  id?: string;
}

export interface IContext {
  '@base': string;
  '@vocab': string;
}

export interface JSONLD<T extends NodeType> {
  '@context': string|Array<string|IContext>;
  '@type': T;
  '@id': string;
  name: string;
}

export const isTypeNode = (type: SchemaType|NodeType) => Object.values(NodeType).includes(type as any);
export const isNode = <T extends SchemaType>({type}: Partial<JSON<T>>) => Object.values(NodeType).includes(type as any);
export const jsonldPath = (type: SchemaType|NodeType, id: string) => \`\${type}/\${id}.jsonld\`;
export const isTypeValid = <T extends SchemaType>({type}: Partial<JSON<T>>) => Object.values(SchemaType).includes(type);
export const isExpandable = (val: any) => !!val && !(val instanceof Date) && typeof val === 'object' && (
  Array.isArray(val) ? val.every(isExpandable) : Object.values(SchemaType).includes(val.type || val['@type'])
);
export const typeToSchemaType = (type?: string) => Object.values(SchemaType).find(val => val.toLowerCase() === (type || '').toLowerCase());

// Sorting

const sortOrdersByType: {
  [type in SchemaType]: {
    [key: string]: {
      order: string;
      type: string;
    };
  };
} = {
  ${classes.map(({ name, sortedProperties }, index) => `${name}: ${JSON.stringify({
    index: {
      order: index.toString().padStart(2, '0'),
      type: 'string'
    },
    ...sortedProperties
  })}`).join(newLineJoin)}
};

const keyDelimiter = '.';

const isNumber = (key: string) => key.match(/^\\d+$/g) !== null;
const keyParts = (key: string) => key.split(keyDelimiter);
const typeLevel = (type: SchemaType) => sortOrdersByType[type].index.order;
const maxLevelByType = (type: any) => sortOrdersByType[type].type.order.length;

const sortOrder = (maxNestedLevel = 1) => (fullKey: string, parentType?: string) => {
  const [keyType, key, ...left] = keyParts(fullKey);
  const keyAsNumber = isNumber(key);
  const nestedKey = [key, ...left].join(keyDelimiter);
  const schemaType = typeToSchemaType(parentType || keyType);
  const { order, type } = keyAsNumber ?
    { order: key.padStart(maxLevelByType(parentType), '0'), type: parentType } :
    typeSortOrder(schemaType, key);
  const nestedOrder = (left.length ? sortOrder(maxNestedLevel)(nestedKey, type) : '');
  const fullOrder = (order + nestedOrder).padEnd(parentType ? 0 : maxNestedLevel * order.length, '0');
  return parentType ? fullOrder : parseInt(typeLevel(schemaType) + fullOrder, 10);
};

export const sortKeysByType = (keys: string[], sort: 'asc' | 'desc' = 'asc') => {
  const maxNestedLevel = keys.map(key => keyParts(key).length).sort().pop();
  const sorter = sortOrder(maxNestedLevel);
  const orders = keys.reduce((prev, key) => ({ ...prev, [key]: sorter(key) }), {});
  return keys.sort((a, b) => sort === 'asc' ? orders[a] - orders[b] : orders[b] - orders[a]);
};

export const typeSortOrder = (type: SchemaType, key: string) => sortOrdersByType[type][key];
`;
  writeFileSync(join(CLASS_DIR, 'types.ts'), content);
};

const generateIndex = (classes: string[]) => {
  const content = `// auto-generated content

export const SCHEMA_VERSION = '${version}';
export * from './types';

${classes.map(name => `export * from './${name}';`).join('\n')}
`;
  writeFileSync(join(CLASS_DIR, 'index.ts'), content);
};

const run = () => {
  const files = listYmlDir();
  const classes = files.map(generateClass);
  generateTypes(classes);
  generateIndex(classes.map(({ name }) => name));
};

run();
