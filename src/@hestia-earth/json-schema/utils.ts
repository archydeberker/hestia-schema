import { readdir, readFile } from 'fs';

export const listFolder = (folder: string) => new Promise<string[]>((resolve, reject) =>
  readdir(folder, (err, data) => err ? reject(err) : resolve(data))
);

export const loadJSONFile = <T>(filepath: string) => new Promise<T>((resolve, reject) =>
  readFile(filepath, 'utf8', (err, data) => err ? reject(err) : resolve(JSON.parse(data) as T))
);
