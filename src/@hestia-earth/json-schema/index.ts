import { join } from 'path';
import { SchemaType, typeToSchemaType } from '@hestia-earth/schema';

import { listFolder, loadJSONFile } from './utils';

const schemaRoot = join(__dirname, 'json-schema');

export type genericType = 'string' | 'boolean' | 'number' | 'integer';
export type definitionPropertyType = 'object' | 'array' | genericType | genericType[] | 'auto';

interface IDefinitionProperty {
  $id?: string;
  type?: definitionPropertyType;
  $comment?: string;
  description?: string;
  title?: string;
  /**
   * Makes this Node unique.
   */
  unique?: boolean;
  /**
   * Can be searched for on the Hestia Platform.
   */
  searchable?: boolean;
  default?: any;
  /**
   * Will be set by Hestia.
   */
  internal?: boolean;
  /**
   * Will be included for existing Node.
   */
  extend?: boolean;
}

interface IDefinitionGeneric extends IDefinitionProperty {
  type: genericType | genericType[];
  enum?: string[];
  const?: string | number | boolean;
}

export interface IDefinitionObject extends IDefinitionProperty {
  type: 'object';
  properties: IDefinitionProperties;
  required?: string[];
  $ref?: string;
  oneOf?: Partial<IDefinitionObject>[];
  anyOf?: Partial<IDefinitionObject>[];
  allOf?: Partial<IDefinitionObject>[];
}

export interface IDefinitionArray extends IDefinitionProperty {
  type: 'array';
  items: definition;
}

export type definition = IDefinitionObject | IDefinitionArray | IDefinitionGeneric;

export interface IDefinitionProperties {
  [key: string]: definition;
}

export interface IDefinition extends IDefinitionObject {
  definitions?: IDefinitionProperties;
}

export type definitions = { [type in SchemaType]: IDefinition };

export const loadSchemas = async () => {
  const files = await listFolder(schemaRoot);
  const schemas = await Promise.all(files.map(async file => ({
    schemaName: file.split('.')[0] as SchemaType,
    schema: await loadJSONFile<IDefinition>(join(schemaRoot, file))
  })));
  return schemas.reduce((prev, { schema, schemaName }) => ({ ...prev, [schemaName]: schema }), {} as definitions);
};

export const uniqueProperties = ({ properties }: IDefinition) =>
  Object.keys(properties).filter(property => properties[property].unique);

export const searchableProperties = ({ properties }: IDefinition) =>
  Object.keys(properties).filter(property => properties[property].searchable);

export const extendableProperties = ({ properties }: IDefinition) =>
  Object.keys(properties).filter(property => properties[property].extend);

const excludedDefaultProperties = [
  '@type',
  'type'
];

export const defaultProperties = ({ properties }: IDefinition) =>
  Object.keys(properties)
    .filter(property => !excludedDefaultProperties.includes(property) && 'default' in properties[property]);

export const refToSchemaType = (ref?: string) =>
  ref ? typeToSchemaType(ref.substring(2).replace('-deep', '').replace('.json#', '')) : undefined;
