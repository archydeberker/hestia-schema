# Hestia JSON Schema

Hestia JSON Schema definitions, used to validate JSON files following hestia schmea.

## Install

```sh
npm config set @hestia-earth:registry https://gitlab.com/api/v4/projects/19965626/packages/npm/
npm install @hestia-earth/json-schema
```
