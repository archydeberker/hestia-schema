import { readdirSync, readFileSync, lstatSync } from 'fs';
import { join } from 'path';

import { validator } from './validate';

const [DOMAIN, folder, forUpload] = process.argv.slice(2);

const domain = `https://${DOMAIN || process.env.DOMAIN || 'www.hestia.earth'}`;
const isUpload = forUpload === 'true'; // if file is for upload, be less strict

const loadFile = (filename: string) => JSON.parse(readFileSync(filename, 'utf8'));

const extensions = [
  'jsonld',
  'json',
  'hestia'
];

const findFiles = (directory: string) => readdirSync(directory)
  .map(path => {
    const isDirectory = lstatSync(join(directory, path)).isDirectory();
    return isDirectory ? findFiles(join(directory, path)) : (
      extensions.includes(path.split('.')[1]) ? join(directory, path) : null
    );
  })
  .flat()
  .filter(Boolean);

export const run = async () => {
  const contentValidator = await validator(domain, isUpload);

  const jsonldFiles = findFiles(folder);

  const results = jsonldFiles
    .map(filepath => {
      const content = loadFile(filepath);
      const nodes = Array.isArray(content) ? content : (
        'nodes' in content ? content.nodes : [content]
      );
      const allErrors = nodes
        .map(node => node['@type'] || node.type ? node : null)
        .filter(Boolean)
        .map(contentValidator)
        .map(({ errors }) => errors);

      return {
        filepath,
        success: !allErrors.some(v => v.length),
        errors: allErrors
      };
    })
    .filter(({ success }) => !success);

  if (results.length) {
    throw new Error(`Validation errors: ${JSON.stringify(results, null, 2)}`);
  }
};
