import * as Ajv from 'ajv';
import { SchemaType } from '@hestia-earth/schema';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

const geojsonSchema = 'http://json.schemastore.org/geojson';

const validateSchemaType = (schemas: definitions, content) => {
  const type = content['@type'] || content.type;
  if (!(type in schemas)) {
    throw new Error(`Unknown or invalid type "${type}"`);
  }
  return true;
};

const validateContent = (ajv: Ajv.Ajv, schemas: definitions) => content => validateSchemaType(schemas, content) && ({
  success: ajv.validate(schemas[content['@type'] || content.type], content),
  errors: ajv.errors || []
});

export const initAjv = () => {
  const ajv = new Ajv({
    schemaId: 'auto',
    allErrors: true
  });
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));
  ajv.addSchema(require('./json-schema/geojson.json'), geojsonSchema);
  return ajv;
};

export const validator = async (domain = 'https://www.hestia.earth', strictMode = true) => {
  const ajv = initAjv();
  const schemas = await loadSchemas();
  Object.keys(schemas).map(schemaName => {
    const schema = schemas[schemaName];
    schema.properties = {
      '@context': {
        type: 'string'
      },
      ...schema.properties
    };
    const isTerm = schemaName === SchemaType.Term || schemaName === `${SchemaType.Term}-deep`;
    schema.oneOf = strictMode && isTerm ? [{ required: ['type', 'id'] }, { required: ['@type', '@id'] }] : schema.oneOf;
    ajv.addSchema(schema, `${domain}/schema/json-schema}/${schemaName}#`);
  });
  return validateContent(ajv, schemas);
};
