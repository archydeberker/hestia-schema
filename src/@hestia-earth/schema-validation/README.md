# Hestia Schema Validation

Module to validate data using the Hestia Schema and Ajv

## Install

```sh
npm config set @hestia-earth:registry https://gitlab.com/api/v4/projects/19965626/packages/npm/
npm install @hestia-earth/schema-validation
```
