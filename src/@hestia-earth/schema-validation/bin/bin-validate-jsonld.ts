#!/usr/bin/env node
import { run } from '../validate-jsonld';

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
