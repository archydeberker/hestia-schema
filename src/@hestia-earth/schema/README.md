# Hestia Schema

Hestia Schema definitions.

## Install

```sh
npm config set @hestia-earth:registry https://gitlab.com/api/v4/projects/19965626/packages/npm/
npm install @hestia-earth/schema
```
