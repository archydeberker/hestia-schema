import { promisify } from 'util';
import { exec } from 'child_process';
import { join, resolve } from 'path';

const ROOT = resolve(join(__dirname, '../'));
const DIST = resolve(ROOT, 'dist');
const FLAGS = ['--registry https://gitlab.com/api/v4/projects/19965626/packages/npm/'];

const promiseExec = promisify(exec);
const scope = '@hestia-earth';

const publish = async (packageName: string) => {
  console.log(`Publishing ${scope}/${packageName}`);
  const packagePath = resolve(DIST, scope, packageName);
  const cmd = `npm publish ${packagePath} ${FLAGS.join(' ')}`;
  try {
    await promiseExec(cmd);
  }
  catch (err) {
    console.error(`Publishing ${packageName} failed: ${cmd}.`);
    throw err;
  }
};

const run = async () => {
  await publish('schema');
  await publish('json-schema');
  await publish('schema-validation');
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
