import { promisify } from 'util';
import { exec } from 'child_process';
import { join, resolve } from 'path';
import { copy, mkdirp, mkdirpSync, pathExists, writeJson } from 'fs-extra';

const promiseExec = promisify(exec);
const scope = '@hestia-earth';

const ROOT = resolve(join(__dirname, '../'));
const PACKAGES_PATH = resolve(join(ROOT, 'src', scope));
const TS_CONFIG = require(resolve(ROOT, 'tsconfig.build.json'));
const BUILD_TMP = resolve(ROOT, '.tmp');
const BUILD_DIST_ROOT = resolve(ROOT, 'dist', scope);

const MODULE_PACKAGE_PATH = resolve(ROOT, 'package.json');
const MODULE_PACKAGE_JSON = require(MODULE_PACKAGE_PATH);
const MODULE_VERSION = MODULE_PACKAGE_JSON.version;

mkdirpSync(BUILD_TMP);

const copyFolders = [
  'json-schema'
];

const buildPlugin = async (packageName: string) => {
  console.log(`Building package: ${packageName}`);
  const PACKAGE_BUILD_DIR = resolve(BUILD_TMP, packageName);

  // create tmp build dir
  await mkdirp(PACKAGE_BUILD_DIR);
  // create dist dir
  await mkdirp(resolve(BUILD_DIST_ROOT, packageName));

  // Write tsconfig.json
  const tsConfig = JSON.parse(JSON.stringify(TS_CONFIG));
  tsConfig.files = [resolve(PACKAGES_PATH, packageName, 'index.ts')];

  // inlcude bin folder if exists
  const binFolder = resolve(PACKAGES_PATH, packageName, 'bin');
  if (await pathExists(binFolder)) {
    tsConfig.include = [join(binFolder, '*.ts')];
  }

  for (const folder of copyFolders) {
    const path = join(packageName, folder);
    if (await pathExists(resolve(PACKAGES_PATH, path))) {
      await copy(
        join(PACKAGES_PATH, path),
        join(BUILD_DIST_ROOT, path)
      );
    }
  }

  const tsConfigPath = resolve(PACKAGE_BUILD_DIR, 'tsconfig.json');
  await writeJson(tsConfigPath, tsConfig);

  // Write package.json
  const pluginPackagePath = resolve(PACKAGES_PATH, packageName, 'package.json');
  const packageJson = require(pluginPackagePath);
  packageJson.name = `${scope}/${packageName}`;
  packageJson.version = MODULE_VERSION;
  await writeJson(resolve(BUILD_DIST_ROOT, packageName, 'package.json'), packageJson, { spaces: 2 });

  await copy(
    join(PACKAGES_PATH, packageName, 'README.md'),
    join(BUILD_DIST_ROOT, packageName, 'README.md')
  );

  const cmd = `tsc -p ${tsConfigPath}`;
  try {
    await promiseExec(cmd, { cwd: ROOT });
  }
  catch (err) {
    console.error(`Building ${packageName} failed: ${cmd}.`);
    throw err;
  }
};

const run = async () => {
  await buildPlugin('schema');
  await buildPlugin('json-schema');
  await buildPlugin('schema-validation');
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
