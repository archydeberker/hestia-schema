import { readdirSync, readFileSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';
import { parse } from 'yamljs';
const version = require(join(__dirname, '..', 'package.json')).version;

const SPACER = '    ';

enum PropertyType {
  array = 'array',
  number = 'number',
  integer = 'integer',
  string = 'string',
  boolean = 'boolean',
  date = 'date',
  'date-time' = 'date-time',
  GeoJSON = 'GeoJSON',
  iri = 'iri'
}

const typeToDefaultValue: {
  [type in PropertyType]?: string;
} = {
  [PropertyType.array]: '[]',
  [PropertyType.string]: "''",
  [PropertyType.boolean]: 'False'
};

export interface IProperty {
  name: string;
  type: PropertyType;
  doc?: string;
  enum?: string[];
  const?: any;
  // default value if none is provided
  default?: any;
  unique?: boolean;
  required?: boolean;
};

export interface IClass {
  name: string;
  type: 'Node'|'Blank Node';
  examples?: string[];
  doc: string;
  properties: IProperty[];
}

export const cleanType = (type: string) => type
  .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(List\[)([a-zA-Z]*)(\])/g, 'array');

const propertyDefault = (type: string) => type in typeToDefaultValue ? typeToDefaultValue[type] : 'None';

const YAML_DIR = join(__dirname, '..', 'yaml');

const ensureDir = (dir: string) => {
  try { mkdirSync(dir, { recursive: true }) } catch (err) { }
};

const readYmlFile = (file: string) => parse(readFileSync(join(YAML_DIR, file), 'utf8')).class as IClass;

const listYmlDir = () => readdirSync(YAML_DIR);

const OUTPUT_DIR = join(__dirname, '..', 'hestia_earth', 'schema');

ensureDir(OUTPUT_DIR);

const cleanEnumName = (name: string) =>
  `${isNaN(+name.charAt(0)) ? '' : '_'}${name.replace(/\-/g, '_').replace(/\s/g, '_')}`;

const propertyIsEnum = (property: Partial<IProperty>) =>
  !!property.enum && property.enum.every(val => typeof val === 'string');

const propertyEnumName = (className: string, { name }: Partial<IProperty>) =>
  `${className}${name.charAt(0).toUpperCase()}${name.substring(1)}`;

const enumValue = (val) => `${cleanEnumName(val).toUpperCase()} = '${val}'`;

const generateProperty = ({ name, type, const: constValue }: IProperty) =>
  `self.fields['${name}'] = ${constValue || propertyDefault(cleanType(type))}`;

const toDict = `${SPACER}def to_dict(self):
${SPACER}${SPACER}values = OrderedDict()
${SPACER}${SPACER}for key, value in self.fields.items():
${SPACER}${SPACER}${SPACER}if (value is not None and value != '' and value != []) or key in self.required:
${SPACER}${SPACER}${SPACER}${SPACER}values[key] = value
${SPACER}${SPACER}return values`;

const classContent = ({ className, isNode, properties }) => `class ${className}:
${SPACER}def __init__(self):
${SPACER}${SPACER}self.required = [${[
  'type',
  ...properties
].filter(({ required }) => required).map(({ name }) => `'${name}'`).join(', ')}]
${SPACER}${SPACER}self.fields = OrderedDict()
${SPACER}${SPACER}self.fields['type'] = ${isNode ? 'NodeType' : 'SchemaType'}.${className.toUpperCase()}.value
${SPACER}${SPACER}${properties.map(generateProperty).join(`\n${SPACER}${SPACER}`)}

${toDict}`;

const jsonldContent = ({ className, isNode, properties }) => `class ${className}JSONLD:
${SPACER}def __init__(self):
${SPACER}${SPACER}self.required = [${[
  '@type',
  ...(isNode ? ['@id'] : []),
  ...properties
].filter(({ required }) => required).map(({ name }) => `'${name}'`).join(', ')}]
${SPACER}${SPACER}self.fields = OrderedDict()
${SPACER}${SPACER}self.fields['@type'] = ${isNode ? 'NodeType' : 'SchemaType'}.${className.toUpperCase()}.value
${SPACER}${SPACER}${[
  isNode ? { name: '@id', type: 'string', required: true } : null,
  ...properties
].filter(Boolean).map(generateProperty).join(`\n${SPACER}${SPACER}`)}

${toDict}`;

const generateEnums = (className: string, properties: IProperty[]) =>
  properties.filter(propertyIsEnum).map(property => `class ${propertyEnumName(className, property)}(Enum):
${SPACER}${property.enum.sort().map(enumValue).join(`\n${SPACER}`)}`);

const getClass = (file: string) => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const properties: IProperty[] = data.properties;
  const hasJSONLD = data.type === 'Node';
  return {
    name: data.name,
    isNode: hasJSONLD,
    properties
  };
};

const writeClasses = (classes: { name: string; isNode: boolean, properties: IProperty[] }[]) => {
  const content = `# auto-generated content
from collections import OrderedDict
from enum import Enum
from pkgutil import extend_path


__path__ = extend_path(__path__, __name__)
SCHEMA_VERSION = '${version}'


class NodeType(Enum):
${SPACER}${classes.filter(({ isNode }) => isNode).map(({ name }) => enumValue(name)).join(`\n${SPACER}`)}


class SchemaType(Enum):
${SPACER}${classes.map(({ name }) => enumValue(name)).join(`\n${SPACER}`)}


${classes.map(({ name: className, isNode, properties }) => [
  ...generateEnums(className, properties),
  classContent({ className, isNode, properties })
].join('\n\n\n')).join(`\n\n\n`)}


${classes.map(({ name: className, isNode, properties }) => jsonldContent({ className, isNode, properties })).join(`\n\n\n`)}
`;
  writeFileSync(join(OUTPUT_DIR, '__init__.py'), content);
};

const run = () => writeClasses(listYmlDir().map(getClass));

run();
