class:
  name: Product
  type: Blank Node
  examples:
    - product.jsonld
  doc: >
    A description of a Product created during the [Cycle].

  properties:
    - name: term
      type: Ref[Term]
      doc: >
        A reference to the [Term] describing the Product.
      required: true
      searchable: true

    - name: description
      type: string
      doc: A short description of the Product.

    - name: variety
      type: string
      doc: The variety (cultivar) of a crop or breed of animal.

    - name: value
      type: array[number]
      minimum: 0
      doc: >
        The quantity of the Product. If an average, it should always be the mean.
        Can be a single number (array of length one) or an array of numbers
        with associated [dates](#dates) (e.g. for multiple harvests in one [Cycle]).
        The units are always specified in the [Term]. For
        crops, value should always be per harvest or per year, following
        [FAOSTAT conventions](http://www.fao.org/economic/the-statistics-division-ess/methodology/methodology-systems/crops-statistics-concepts-definitions-and-classifications/en/).

    - name: sd
      type: array[number]
      minimum: 0
      doc: The standard deviation of value.

    - name: min
      type: array[number]
      doc: The minimum of value.

    - name: max
      type: array[number]
      doc: The maximum of value.

    - name: statsDefinition
      type: string
      enum:
        - organisations
        - sites
        - cycles
        - replications
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (sd, min, max, and value) are calculated across
        (either organisations, sites, cycles, or replications), calculated
        across some spatial area or regions, simulated (e.g. using Monte-Carlo simulation)
        or derived from a model.

    - name: observations
      type: array[number]
      doc: >
        The number of observations the descriptive statistics are calculated over, if different
        from the [numberOfCycles](./Cycle#numberOfCycles) specified in [Cycle].

    - name: date
      type: array[string]
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        A corresponding array to [value](#value), representing the dates of the Product
        in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        For Products created over periods, the start date of the Product (if different from the
        start date of the [Cycle]) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        For Products created over periods, the end date of the Product (if different from the start
        date of the [Cycle]) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: price
      type: number
      minimum: 0
      doc: The sale price of this Product per kilogram, in local currency units.

    - name: currency
      type: string
      pattern: ^[A-Z]{3}$
      doc: The three letter [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217) currency code.

    - name: revenue
      type: number
      minimum: 0
      doc: The total revenue (price x [quantity](#value)) of this Product.

    - name: economicValueShare
      type: number
      minimum: 0
      maximum: 100
      doc: >
        The economic value (typically revenue) of this Product, divided by the
        total economic value of all Products, expressed as a percentage.

    - name: primary
      type: boolean
      doc: >
        Where the are multiple products, whether this product is the primary product.
        Defaults to true if there is only one product or if economicValueShare > 50.

    - name: properties
      type: List[Property]
      doc: >
        A list of [Properties](./Property) of the Product, which would override any
        default properties specified in [Term]. For crops, dry matter is
        a default property of the Term and can be changed by adding
        [dry matter](/term/dryMatter) here.

    - name: reliability
      type: integer
      minimum: 1
      maximum: 5
      doc: >
        An assessment of the reliability of these data following the pedigree matrix approach
        detailed by [Edelen & Ingwersen (2019)](https://cfpub.epa.gov/si/si_public_file_download.cfm?p_download_id=528687).
        (1) Verified (e.g. by on site checks, recalculation, or mass balance estimation) data
        based on measurements; (2) Verified data based on a calculation or non-verified data
        based on measurements; (3) Non-verified data based on a calculation; (4) Documented
        estimate; (5) Undocumented estimate. In different language, (1-2) can be considered
        primary data and (3-5) secondary.

    - name: methodModel
      type: Ref[Term]
      doc: >
        A reference to the [Term] describing the method or model for acquiring or estimating
        these data.
      searchable: true

    - name: methodModelDescription
      type: string
      doc: >
        A free text field, describing the method or model used for acquiring or estimating
        these data.

    - name: source
      type: Ref[Source]
      doc: A reference to the [Source] of these data, if different from the Source of the [Cycle].

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: A list of fields that have been 'aggregated' using data from multiple [Sites](./Site) and [Cycles](./Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - animalProduct
                    - crop
                    - cropResidue
                    - liveAnimal
                    - liveAquaticSpecies
                    - processedFood
      - if:
          required:
            - methodModel
        then:
          properties:
            method:
              properties:
                termType:
                  enum:
                    - model
                    - methodEmissionResourceUse
