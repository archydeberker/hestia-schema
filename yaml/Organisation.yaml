class:
  name: Organisation
  type: Node
  examples:
    - organisation.jsonld
  doc: >
    An Organisation could be a farm, a food processor, food packaging producer, a
    retailer, or a research organisation. Organisations are useful to indicate who
    manages [Sites](./Site).

  properties:
    - name: name
      type: string
      doc: A short name for the Organisation.
      extend: true
      searchable: true

    - name: boundary
      type: GeoJSON
      doc: >
        A nested [GeoJSON](https://tools.ietf.org/html/rfc7946) object for the Organisation
        boundary of type 'FeatureCollection'.

    - name: area
      type: number
      exclusiveMinimum: 0
      doc: The area of the Organisation in hectares.

    - name: latitude
      type: number
      minimum: -90
      maximum: 90
      doc: The latitude of the Organisation (-90 to 90).
      searchable: true

    - name: longitude
      type: number
      minimum: -180
      maximum: 180
      doc: The longitude of the Organisation (-180 to 180).
      searchable: true

    - name: streetAddress
      type: string
      doc: The street address.

    - name: city
      type: string
      doc: The city or town.

    - name: region
      type: Ref[Term]
      doc: >
        The lowest geographical level [GADM](/glossary?termType=region) region.
        For example the district name [Cherwell](/term/GADM-GBR.1.69.1_1)
        or the id [GADM-GBR.1.69.1_1](/term/GADM-GBR.1.69.1_1)
        could be provided and Hestia will determine that it is also in the region
        [Oxfordshire](/term/GADM-GBR.1.69_1).
      searchable: true

    - name: country
      type: Ref[Term]
      doc: The country name from the [Glossary](/glossary?termType=region).
      searchable: true
      required: true

    - name: postOfficeBoxNumber
      type: string
      doc: The post office box number.

    - name: postalCode
      type: string
      doc: The postal code.

    - name: website
      type: iri
      doc: A link to a website describing the Actor.

    - name: glnNumber
      type: string
      pattern: ^\d{13}$
      doc: The [Global Location Number](https://en.wikipedia.org/wiki/Global_Location_Number).

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        The start date of the Organisation in
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD,
        YYYY-MM, or YYYY).

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        The end date of the Organisation in
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD,
        YYYY-MM, or YYYY).

    - name: originalId
      type: string
      doc: The identifier for these data in the source database.
      internal: true

    - name: uploadBy
      type: Ref[Actor]
      doc: >
        The user who uploaded these data to Hestia.
      internal: true
      required: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: dataPrivate
      type: boolean
      doc: If these data are private.
      required: true
      default: false

  validation:
    allOf:
      - if:
          required:
            - country
        then:
          properties:
            country:
              properties:
                termType:
                  enum:
                    - region
      - if:
          required:
            - region
        then:
          properties:
            region:
              properties:
                termType:
                  enum:
                    - region
