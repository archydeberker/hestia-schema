class:
  name: Completeness
  type: Blank Node
  examples:
    - completeness.jsonld
  doc: >
    A specification of how complete the [Inputs](./Input) and [Products](./Product) data are.
    Each field in Completeness corresponds to one or more [termTypes](./Term#termType) in the
    Glossary. If a termType is marked "complete", but there are no Inputs or Products of that
    termType in the Cycle, then the quantity of these Inputs and Products equals zero. If a
    termType is not relavent for a Cycle, mark it as complete (e.g., soilAmendments
    would be marked complete for a food processing Cycle, where none are used).

  properties:
    - name: electricityFuel
      type: boolean
      doc: >
        All [electricity](/glossary?termType=electricity) and [fuel](/glossary?termType=fuel),
        which is used during the [Cycle], is recorded.
      required: true

    - name: material
      type: boolean
      doc: >
        All [material](/glossary?termType=material) Inputs, which includes infrastructure use
        depreciated over the [Cycle], are recorded.
      required: true

    - name: fertilizer
      type: boolean
      doc: >
        All [organic](/glossary?termType=organicFertilizer) and
        [inorganic](/glossary?termType=inorganicFertilizer) fertilizers used during the
        [Cycle] are recorded.
      required: true

    - name: other
      type: boolean
      doc: >
        All [other](/glossary?termType=other) Inputs, which includes [seed](/term/seed),
        are recorded.
      required: true

    - name: pesticidesAntibiotics
      type: boolean
      doc: >
        All pesticides (either as [active ingredients](/glossary?termType=pesticideAI)
        or [brand names](/glossary?termType=pesticideBrandName)) and
        [antibiotics](/glossary?termType=antibiotic) used during the [Cycle] are recorded.
      required: true

    - name: soilAmendments
      type: boolean
      doc: >
        All [soil amendments](/glossary?termType=soilAmendment) used during the [Cycle]
        are recorded.
      required: true

    - name: water
      type: boolean
      doc: >
        The amount of [water](/glossary?termType=water) used during the [Cycle] is
        recorded.
      required: true

    - name: products
      type: boolean
      doc: >
        All Products produced during the [Cycle] are recorded, whether they are
        [crops](/glossary?termType=crop), [live animals](/glossary?termType=liveAnimal),
        [live aquatic species](/glossary?termType=liveAquaticSpecies),
        [animal products](/glossary?termType=animalProduct), or
        [processed foods](/glossary?termType=processedFood).
      required: true

    - name: coProducts
      type: boolean
      doc: >
        All co-products produced during the [Cycle], and their relative economic values
        are recorded. These are part of the the [crop](/glossary?termType=crop),
        [animal products](/glossary?termType=animalProduct), and
        [processed foods](/glossary?termType=processedFood) termTypes.
      required: true

    - name: cropResidue
      type: boolean
      doc: >
        The quantities of above and below ground
        [crop residue](/glossary?termType=cropResidue) created during the [Cycle] are
        recorded.
      required: true

    - name: manureManagement
      type: boolean
      doc: >
        The [manure management](/glossary?termType=manureManagement) systems used are
        specified. For animal product Cycles only. For other Cycles, including crop
        production, set to true.
      required: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true
