class:
  name: Term
  type: Node
  examples:
    - term.jsonld
    - term2.jsonld
    - term3.jsonld
  doc: >
    A Term is a name from the [Glossary of Terms](/glossary/) (controlled vocabulary)
    that is used to ensure the most important data fields describing the agri-food
    system are named consistently.

  properties:
    - name: name
      type: string
      doc: The name of the Term.
      extend: true
      required: true
      searchable: true
      unique: true

    - name: synonyms
      type: List[string]
      doc: A list of synonyms for the name of the Term.
      searchable: true

    - name: definition
      type: string
      doc: A definition of the Term.

    - name: description
      type: string
      doc: A full description of the Term, which can include source data.

    - name: units
      type: string
      doc: >
        The units that the value (quantity) must always be expressed in (e.g. kg).
      extend: true
      searchable: true

    - name: subClassOf
      type: List[Ref[Term]]
      doc: >
        A reference to the Term that is one level above in a hierarchy (see the
        [RDF Vocabulary](https://www.w3.org/TR/owl-ref/#subClassOf-def) for more details).

    - name: defaultProperties
      type: List[Property]
      doc: >
        A list of default [Properties](./Property) of the Term (e.g. the dry matter
        of a crop).
      searchable: true

    - name: casNumber
      type: string
      pattern: ^[\d\-]+$
      doc: >
        The unique numerical identifier assigned by the Chemical Abstracts Service (CAS)
        to every chemical substance described in the open scientific literature.

    - name: ecoinventActivityId
      type: string
      doc: The identifier for the activity in the [EcoInvent](https://www.ecoinvent.org/) database.

    - name: fishstatName
      type: string
      doc: >
        The name of the species in the FAO
        [FISHSTAT](http://www.fao.org/fishery/statistics/global-aquaculture-production/en) database.

    - name: hsCode
      type: string
      doc: The World Customs Organization [Harmonized System 2017](http://www.wcoomd.org/en/topics/nomenclature/instrument-and-tools/hs-nomenclature-2017-edition/hs-nomenclature-2017-edition.aspx) code.

    - name: iccCode
      type: integer
      doc: The [Indicative Crop Classification](https://unstats.un.org/unsd/classifications/Family/Detail/1002) code.

    - name: iso31662Code
      type: string
      pattern: ^[A-Z]{2}-?([A-Za-z\d]{1,3})?$
      doc: The [ISO 3166-2](https://en.wikipedia.org/wiki/ISO_3166-2) code for sub divisions within countries.

    - name: gadmFullName
      type: string
      doc: >
        The full name of the administrative region including all higher level region names in
        the [GADM](https://gadm.org/) database.

    - name: gadmId
      type: string
      pattern: ^[A-Z]{3}[\.]?([\d_\.]*)?$
      doc: The unique identifier assigned by [GADM](https://gadm.org/) database.

    - name: gadmLevel
      type: integer
      minimum: 0
      maximum: 5
      doc: The level of the administrative region in the [GADM](https://gadm.org/) database.
      searchable: true

    - name: gadmName
      type: string
      doc: The name of the administrative region in the [GADM](https://gadm.org/) database.
      searchable: true

    - name: gadmCountry
      type: string
      doc: The name of the country in the [GADM](https://gadm.org/) database.
      searchable: true

    - name: latitude
      type: number
      minimum: -90
      maximum: 90
      doc: The latitude (-90 to 90, WGS84 datum). If a polygon, the centroid.
      searchable: true

    - name: longitude
      type: number
      minimum: -180
      maximum: 180
      doc: The longitude (-180 to 180, WGS84 datum). If a polygon, the centroid.
      searchable: true

    - name: gtin
      type: string
      pattern: ^[\d]{8,14}$
      doc: The Global Trade Item Number (GTIN) is an identifier for trade items, developed by GS1.

    - name: scientificName
      type: string
      doc: The taxonomic name of an organism that consists of the genus and species.
      searchable: true

    - name: website
      type: iri
      doc: A website URL.

    - name: agrovoc
      type: iri
      doc: >
        A hyperlink to the [FAO AGROVOC](http://aims.fao.org/standards/agrovoc/functionalities/search)
        multilingual thesaurus entry.

    - name: aquastatSpeciesFactSheet
      type: iri
      pattern: ^http(s?):\/\/www\.fao\.org\/
      doc: A hyperlink to the [AQUASTAT](http://www.fao.org/fishery/species/search/en) species fact sheet.

    - name: chemidplus
      type: iri
      pattern: ^https:\/\/chem\.nlm\.nih\.gov\/
      doc: A hyperlink to the [ChemIDplus](https://chem.nlm.nih.gov/chemidplus/) page.

    - name: feedipedia
      type: iri
      pattern: ^https:\/\/www\.feedipedia\.org\/
      doc: A hyperlink to the [Feedipedia](https://www.feedipedia.org/) page.

    - name: fishbase
      type: iri
      pattern: ^https:\/\/www\.fishbase.\se\/
      doc: A hyperlink to the [Fishbase](https://www.fishbase.se) page.

    - name: pubchem
      type: iri
      pattern: ^https:\/\/pubchem\.ncbi\.nlm\.nih\.gov\/
      doc: A hyperlink to the [PubChem](https://pubchem.ncbi.nlm.nih.gov/) page.

    - name: wikipedia
      type: iri
      pattern: ^https:\/\/en\.wikipedia\.org\/
      doc: A hyperlink to the [Wikipedia](https://www.wikipedia.org/) page.

    - name: termType
      type: string
      enum:
        - animalProduct
        - animalManagement
        - aquacultureManagement
        - antibiotic
        - biodiversity
        - building
        - characterisedIndicator
        - crop
        - cropEstablishment
        - cropProtection
        - cropResidue
        - cropResidueManagement
        - cropSupport
        - electricity
        - emission
        - fuel
        - inorganicFertilizer
        - irrigation
        - landUseManagement
        - liveAnimal
        - liveAquaticSpecies
        - machinery
        - manureManagement
        - material
        - measurement
        - methodEmissionResourceUse
        - methodMeasurement
        - model
        - organicFertilizer
        - other
        - pesticideAI
        - pesticideBrandName
        - processedFood
        - property
        - region
        - resourceUse
        - soilAmendment
        - soilTexture
        - soilType
        - standardsLabels
        - system
        - transport
        - usdaSoilType
        - water
        - waterRegime
      doc: The type of term.
      extend: true
      required: true
      searchable: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - iccCode
        then:
          properties:
            termType:
              enum:
                - crop
      - if:
          anyOf:
            - required:
              - aquastatSpeciesFactSheet
            - required:
              - fishbase
            - required:
              - fishstatName
        then:
          properties:
            termType:
              enum:
                - liveAquaticSpecies
      - if:
          anyOf:
            - required:
              - iso31662Code
            - required:
              - gadmFullName
            - required:
              - gadmLevel
            - required:
              - gadmName
            - required:
              - gadmId
            - required:
              - latitude
            - required:
              - longitude
        then:
          properties:
            termType:
              enum:
                - region
