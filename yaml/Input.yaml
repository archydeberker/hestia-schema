class:
  name: Input
  type: Blank Node
  examples:
    - input.jsonld
  doc: A description of an Input used during the [Cycle].

  properties:
    - name: term
      type: Ref[Term]
      doc: A reference to the [Term] describing the Input.
      required: true
      searchable: true

    - name: description
      type: string
      doc: A short description of the Input.

    - name: value
      type: array[number]
      doc: >
        The quantity of the Input. If an average, it should always be the mean.
        Can be a single number (array of length one) or an array of numbers
        with associated [dates](#dates) (e.g. representing an application schedule).

    - name: sd
      type: array[number]
      minimum: 0
      doc: The standard deviation of value.

    - name: min
      type: array[number]
      doc: The minimum of value.

    - name: max
      type: array[number]
      doc: The maximum of value.

    - name: statsDefinition
      type: string
      enum:
        - organisations
        - sites
        - cycles
        - replications
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (sd, min, max, and value) are calculated across
        (either organisations, sites, cycles, or replications), calculated
        across some spatial area or regions, simulated (e.g. using Monte-Carlo simulation)
        or derived from a model.

    - name: observations
      type: array[number]
      doc: >
        The number of observations the descriptive statistics are calculated over, if different
        from the [numberOfCycles](./Cycle#numberOfCycles) specified in [Cycle].

    - name: dates
      type: array[string]
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        A corresponding array to [value](#value), representing the dates of the Input
        in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        For Inputs over periods different to [Cycle], the start date of the Input (if different from the
        start date of the [Cycle]) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        For Inputs over periods different to [Cycle], the end date of the Input (if different from the start
        date of the [Cycle]) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).

    - name: inputDuration
      type: number
      doc: The duration of the Input in days.

    - name: currency
      type: string
      minLength: 3
      maxLength: 3
      doc: >
        The three letter currency code in [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)
        format.

    - name: price
      type: number
      minimum: 0
      doc: The cost of this product, in local currency units.

    - name: cost
      type: number
      minimum: 0
      doc: The total cost of this product per functional unit, expressed as a positive value.

    - name: properties
      type: List[Property]
      doc: >
        A list of [Properties](./Property) of the Input, which would override any
        default properties specified in [Term].

    - name: reliability
      type: integer
      minimum: 1
      maximum: 5
      doc: >
        An assessment of the reliability of these data following the pedigree matrix approach
        detailed by [Edelen & Ingwersen (2019)](https://cfpub.epa.gov/si/si_public_file_download.cfm?p_download_id=528687).
        (1) Verified (e.g. by on site checks, recalculation, or mass balance estimation) data
        based on measurements; (2) Verified data based on a calculation or non-verified data
        based on measurements; (3) Non-verified data based on a calculation; (4) Documented
        estimate; (5) Undocumented estimate. In different language, (1-2) can be considered
        primary data and (3-5) secondary.

    - name: methodModel
      type: Ref[Term]
      doc: >
        A reference to the [Term] describing the method or model for acquiring or estimating
        these data.
      searchable: true

    - name: methodModelDescription
      type: string
      doc: >
        A free text field, describing the method or model used for acquiring or estimating
        these data.

    - name: impactAssessment
      type: Ref[ImpactAssessment]
      doc: >
        A reference to the node containing environmental impact data related to
        producing this product and transporting it to the [Site].

    - name: source
      type: Ref[Source]
      doc: A reference to the [Source] of these data, if different from the Source of the [Cycle].

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: A list of fields that have been 'aggregated' using data from multiple [Sites](./Site) and [Cycles](./Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - antibiotic
                    - electricity
                    - fuel
                    - material
                    - inorganicFertilizer
                    - organicFertilizer
                    - pesticideAI
                    - pesticideBrandName
                    - other
                    - soilAmendment
                    - water
                    - animalProduct
                    - crop
                    - liveAnimal
                    - liveAquaticSpecies
                    - processedFood
      - if:
          required:
            - methodModel
        then:
          properties:
            method:
              properties:
                termType:
                  enum:
                    - model
                    - methodEmissionResourceUse
