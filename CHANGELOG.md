# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.16.0...v3.0.0) (2021-04-16)


### ⚠ BREAKING CHANGES

* `date` was renamed to `dates` on every Blank Node
* `gapFilled` and `recalculated` fields are merged in `added` and `updated`.

### Features

* add `modelled` option to `statsDefinition` ([2264549](https://gitlab.com/hestia-earth/hestia-schema/commit/2264549b40d3ecfb2680230850ccae49b6ea5785))
* replace `gapFilled` and `recalculated` with `added` and `updated` ([3c4d3eb](https://gitlab.com/hestia-earth/hestia-schema/commit/3c4d3eb272109bc00c139aae2d4de9cf9210f20b))
* **blank nodes:** add or rename fields `methodModel` and `methodModelDescription` ([c0d9832](https://gitlab.com/hestia-earth/hestia-schema/commit/c0d98329551739d379652aa3d7d9e90c27fbd8dc))


### Bug Fixes

* change `date` to `dates` on Blank Nodes ([d5f61ae](https://gitlab.com/hestia-earth/hestia-schema/commit/d5f61aef697a496675c857c7f38417e1a8b5dfe9))

## [2.16.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.15.0...v2.16.0) (2021-04-08)


### Features

* **indicator:** add further `statsDefinition` options ([f7628d8](https://gitlab.com/hestia-earth/hestia-schema/commit/f7628d81f778e98c16e6d4a7987bb74463fedc16))

## [2.15.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.14.0...v2.15.0) (2021-04-07)


### Features

* **impact assessment:** index `emissionsResourceUse` and `impacts` ([ff6c45c](https://gitlab.com/hestia-earth/hestia-schema/commit/ff6c45cd75588c43ad8ae77f062aeb077d599470))

## [2.14.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.13.1...v2.14.0) (2021-03-31)


### Features

* add `spatial` and `regions` options to `statsDefinition` ([36cfbdb](https://gitlab.com/hestia-earth/hestia-schema/commit/36cfbdb3af0496646e5f361b7a6d3bb2fb9d323d))
* **impact assessment:** add `organic` and `irrigated` fields ([f3c4d35](https://gitlab.com/hestia-earth/hestia-schema/commit/f3c4d3597562c5553633c500e73299c781df78a2))
* **impact assessment:** allow `characterisedIndicator` terms in `emissionsResourceUse` ([f7d30fb](https://gitlab.com/hestia-earth/hestia-schema/commit/f7d30fbf4334c729a45a028b8e0c66eb6bbd63a4))
* **indicator:** add `distribution` field ([d618a98](https://gitlab.com/hestia-earth/hestia-schema/commit/d618a980437ce1ce6918707dd9a5903c7e8b1851))

### [2.13.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.13.0...v2.13.1) (2021-03-26)


### Bug Fixes

* **schema validation:** fix conditional validation on arrays items ([9f473c2](https://gitlab.com/hestia-earth/hestia-schema/commit/9f473c238e0a462138edee0403e58460f9f3c126))

## [2.13.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.12.0...v2.13.0) (2021-03-17)


### Features

* **impactAssessment:** allow `kWh` functional unit ([59889f5](https://gitlab.com/hestia-earth/hestia-schema/commit/59889f545a44e28ca006471f4050f69d4aec8186))

## [2.12.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.11.0...v2.12.0) (2021-03-16)


### Features

* add `description` field to Infrastructure and Measurement ([edb59aa](https://gitlab.com/hestia-earth/hestia-schema/commit/edb59aa764325e4db36b7a2078ff1acdb47b8882))
* **emission:** add `depth` field for soil emissions ([3b5c89e](https://gitlab.com/hestia-earth/hestia-schema/commit/3b5c89eee0ef0207415e0de96edd21c63b4f9715))
* **infrastructure:** add `impactAssessment` term ([d791dc9](https://gitlab.com/hestia-earth/hestia-schema/commit/d791dc95925f515cc0a63f402299722e7fa148ea))
* **json-schema:** handle `pattern` on arrays ([da8a7be](https://gitlab.com/hestia-earth/hestia-schema/commit/da8a7be0034ffb2aa77bb2d5f503583503e5d736))
* **property:** add `description` field ([b03f696](https://gitlab.com/hestia-earth/hestia-schema/commit/b03f696c16289fbc0723aae88355ae71f5da7f28))


### Bug Fixes

* **schema:** handle sorting between different node types ([a2dc47a](https://gitlab.com/hestia-earth/hestia-schema/commit/a2dc47af1b6671261650bca9526e810f28446b97))

## [2.11.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.10.0...v2.11.0) (2021-03-11)


### Features

* **schema:** add method to sort node keys by type ([57cbba6](https://gitlab.com/hestia-earth/hestia-schema/commit/57cbba6a8b3ca1c28ebca518c01bba773fdb4be5))

## [2.10.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.9.0...v2.10.0) (2021-03-08)


### Features

* add `schemaVersion` to every jsonld created ([a4a6d68](https://gitlab.com/hestia-earth/hestia-schema/commit/a4a6d687aa077141605674792089eee0bbd2493d))
* replace `relDays` in blank nodes to `date` field ([9a7ac30](https://gitlab.com/hestia-earth/hestia-schema/commit/9a7ac3015732c91a7df00dc15cac5ded8eb1b592)), closes [#80](https://gitlab.com/hestia-earth/hestia-schema/issues/80)
* **bibliography:** add `articlePdf` field ([40d0d4c](https://gitlab.com/hestia-earth/hestia-schema/commit/40d0d4cf10ca4cd854bfe32886dc803ce27ca3f5))
* **blank nodes:** convert `dataState` to lists of fields ([ee7657a](https://gitlab.com/hestia-earth/hestia-schema/commit/ee7657ae42d5f0cefadb03724bac8a75ba698cf3)), closes [#65](https://gitlab.com/hestia-earth/hestia-schema/issues/65)

## [2.9.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.8.0...v2.9.0) (2021-03-02)


### Features

* **property:** remove `valueType` field ([682869e](https://gitlab.com/hestia-earth/hestia-schema/commit/682869e35c4783c3fd4f410369655c46e2f595ca)), closes [#71](https://gitlab.com/hestia-earth/hestia-schema/issues/71)
* **site:** add awareWaterBasinId field ([16c86dd](https://gitlab.com/hestia-earth/hestia-schema/commit/16c86dd412a62391f82e7766bf9378470a0a43a1))
* **term:** make `gadmName` searchable ([dcf4cf7](https://gitlab.com/hestia-earth/hestia-schema/commit/dcf4cf790ea9f7627699ef6d4a698e01bd9f8c46))
* **term:** remove `termType=ecoregion` and `ecoregionCode` field ([a88e32f](https://gitlab.com/hestia-earth/hestia-schema/commit/a88e32f9f53ff968d54fb9bcf112e2d39e740522)), closes [#78](https://gitlab.com/hestia-earth/hestia-schema/issues/78)

## [2.8.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.7.0...v2.8.0) (2021-02-27)


### Features

* **impactAssessment:** add `startDate` ([57decda](https://gitlab.com/hestia-earth/hestia-schema/commit/57decda651b0810e6cd5e01f8ae1baf33a681e96))


### Bug Fixes

* **bibliography:** remove `documentType` field ([2242074](https://gitlab.com/hestia-earth/hestia-schema/commit/2242074e6fc33fd35ebe1cf027e3ffac03e12aba))
* **cycle:** set `cycleDuration` as not required ([2746a40](https://gitlab.com/hestia-earth/hestia-schema/commit/2746a407e8798f4baa2710b9cf252602fc28654c)), closes [#68](https://gitlab.com/hestia-earth/hestia-schema/issues/68)

## [2.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.6.1...v2.7.0) (2021-02-25)


### Features

* **cycle:** increase upload size limit to `2000` ([853fbeb](https://gitlab.com/hestia-earth/hestia-schema/commit/853fbebcc64efd945dffb88e40abb83afcadd3b0))
* **term:** add `gadmCountry` ([04d75e7](https://gitlab.com/hestia-earth/hestia-schema/commit/04d75e7e3a1c62900db0b161f0437c2bf0874b17))

### [2.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.6.0...v2.6.1) (2021-02-18)


### Bug Fixes

* **term:** fix pattern for `gadmId` ([d34fb00](https://gitlab.com/hestia-earth/hestia-schema/commit/d34fb00a4f6dcdfb42b2130b9ba85d3e3bacb004))

## [2.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.5.0...v2.6.0) (2021-02-18)


### Features

* add new `termTypes` `antibiotic` and `cropResidue` ([7986161](https://gitlab.com/hestia-earth/hestia-schema/commit/79861615f642fe714cea3bdbf586abf4a8442493)), closes [#72](https://gitlab.com/hestia-earth/hestia-schema/issues/72)
* **completeness:** link fields to termTypes ([abf8b2e](https://gitlab.com/hestia-earth/hestia-schema/commit/abf8b2ee75379bf0fd0643f88e09d347aad355b6))
* **cycle:** set `cycleDuration` to `required` with `default` = `365` ([f81a9d2](https://gitlab.com/hestia-earth/hestia-schema/commit/f81a9d269f19f7d0a4ff986e4d85616ab1a7d9c4)), closes [#68](https://gitlab.com/hestia-earth/hestia-schema/issues/68)
* **property:** index `key` ([eaeb1ba](https://gitlab.com/hestia-earth/hestia-schema/commit/eaeb1ba7aa1559157a91aa6e157abab76ae5996e))
* **property:** index `value` ([9e026f8](https://gitlab.com/hestia-earth/hestia-schema/commit/9e026f860133f27352ad515fe62b6f1ff86de697))


### Bug Fixes

* **property:** remove `string` type for automatic value type detection ([fd0598d](https://gitlab.com/hestia-earth/hestia-schema/commit/fd0598d03e6c78f6d22baa2cf81109ee409f4e26))

## [2.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.4.0...v2.5.0) (2021-02-17)


### Features

* **term:** set `defaultProperties` as `searchable` ([cd45b97](https://gitlab.com/hestia-earth/hestia-schema/commit/cd45b973c5063a2545f98edbbee5547e67b03cb9))


### Bug Fixes

* **term:** fix pattern for `casNumber` ([2e3bbcd](https://gitlab.com/hestia-earth/hestia-schema/commit/2e3bbcd74d3f47f4f23336f30290ab0421a92f6f))
* **term:** remove regex on `ecoregionCode` ([c0f6b9a](https://gitlab.com/hestia-earth/hestia-schema/commit/c0f6b9ad3944560e697a797685f187bb5d41b259))

## [2.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.3.0...v2.4.0) (2021-02-17)


### Features

* **completeness:** add dataState and dataVersion ([a9130e6](https://gitlab.com/hestia-earth/hestia-schema/commit/a9130e64e5cd11d3408b68a9f0aad3b745bc434f))
* **cycle:** add `dataDescription` field ([bad777d](https://gitlab.com/hestia-earth/hestia-schema/commit/bad777deac2fb1e1f437afe2af48fb26ce7d800f)), closes [#70](https://gitlab.com/hestia-earth/hestia-schema/issues/70)
* **term:** add `latitude` and `longitude` ([3bfa03b](https://gitlab.com/hestia-earth/hestia-schema/commit/3bfa03be00082c7d452e1bf5fca50ee836cb01b1))
* **term:** add `termType` validation on fields ([49d4c87](https://gitlab.com/hestia-earth/hestia-schema/commit/49d4c871be2218b09a79740ac86368076c863de6))

## [2.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.2.0...v2.3.0) (2021-02-17)


### Features

* **site:** index latitude / longitude ([a4165ae](https://gitlab.com/hestia-earth/hestia-schema/commit/a4165ae97cc3999f29e7a319301b4da10333c333))

## [2.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.1.0...v2.2.0) (2021-02-15)


### Features

* **emission:** set `value` as `searchable` ([39940d9](https://gitlab.com/hestia-earth/hestia-schema/commit/39940d96db1f47861afcce312973d50e72abf1c1))
* **product:** add variety as field ([3ec4293](https://gitlab.com/hestia-earth/hestia-schema/commit/3ec4293306e3c03adfc4310d90ace109f3397ab5)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)
* **property:** remove redundant fields and description ([4ef6b96](https://gitlab.com/hestia-earth/hestia-schema/commit/4ef6b965451313c586aaa6ea6f97836383e9f42b)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)
* **term:** move properties to fields ([11f932c](https://gitlab.com/hestia-earth/hestia-schema/commit/11f932cc84ff55c87eaa00c666067be80aa06936)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)

## [2.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.0.0...v2.1.0) (2021-02-11)


### Features

* **schema-validation:** export function `initAjv` ([dfdb0a0](https://gitlab.com/hestia-earth/hestia-schema/commit/dfdb0a03673d7602f2c5ab26645fc65947661fc4))
* **source:** set max upload limit to `200` ([384f5a6](https://gitlab.com/hestia-earth/hestia-schema/commit/384f5a68d840491d7bd88ba2c1417aa22aec23af))


### Bug Fixes

* **schema:** remove unused `jsonId` function ([5f13fc7](https://gitlab.com/hestia-earth/hestia-schema/commit/5f13fc704bac2a0d8690636c53c51f4c7101d9e5))

## [2.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.2...v2.0.0) (2021-02-09)


### ⚠ BREAKING CHANGES

* **term:** `Term.termType == 'method'` has been replaced
* install `@hestia-earth/json-schema` to load the schemas

### Features

* **term:** split `method` `termType` into methods for emissions and methods for measurements ([a6c6baa](https://gitlab.com/hestia-earth/hestia-schema/commit/a6c6baa57e12d19fc44d14fc1e7fee506ed5cb7e))


* split json-schema into its own module ([b0ca606](https://gitlab.com/hestia-earth/hestia-schema/commit/b0ca606db0a8b6b5247b4d19c1d53e7bb551e874))

### [1.2.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.1...v1.2.2) (2021-02-06)

### [1.2.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.0...v1.2.1) (2021-02-06)

## [1.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.1.0...v1.2.0) (2021-02-06)


### Features

* add number of observations field for descriptive statistics ([c4411e3](https://gitlab.com/hestia-earth/hestia-schema/commit/c4411e308aa2bc661fba28c4252be96e49cc8c26)), closes [#58](https://gitlab.com/hestia-earth/hestia-schema/issues/58)
* change descriptive statistics fields to type array ([9352c10](https://gitlab.com/hestia-earth/hestia-schema/commit/9352c10f4dd04b4e31657ef0a7ed81f760120e5e)), closes [#67](https://gitlab.com/hestia-earth/hestia-schema/issues/67)
* **impact assessment:** add `autoGenerated` field ([cb3a760](https://gitlab.com/hestia-earth/hestia-schema/commit/cb3a7605582c3798ad6d0e962863b9c39c33d88d))

## [1.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.0.1...v1.1.0) (2021-02-05)


### Features

* **site:** add standard deviation of area field ([eafb664](https://gitlab.com/hestia-earth/hestia-schema/commit/eafb66427995758fc987341b6a3f57e5c8545fdc)), closes [#57](https://gitlab.com/hestia-earth/hestia-schema/issues/57)


### Bug Fixes

* **actor:** remove `required` on `email` ([cd63803](https://gitlab.com/hestia-earth/hestia-schema/commit/cd63803ea900a0be08bb2e3ace7495857658713e))
* **emission:** allow `transport` in `inputs` ([60e4ab5](https://gitlab.com/hestia-earth/hestia-schema/commit/60e4ab51c53611f9e1b3e6c7c6ccd75e75e0caa7))

### [1.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.0.0...v1.0.1) (2021-02-05)


### Bug Fixes

* set properties to extend ([9aad40c](https://gitlab.com/hestia-earth/hestia-schema/commit/9aad40c835ffa5cf752d40d86baa68445fd320e9))

## [1.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.5...v1.0.0) (2021-02-01)


### Bug Fixes

* **navbar:** fix alignment ([df56c05](https://gitlab.com/hestia-earth/hestia-schema/commit/df56c05824a21fa413a9fb00069295a056026767))

### [0.6.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.4...v0.6.5) (2021-01-29)


### Features

* **cycle:** add validation between `siteType` and `functionalUnitMeasure` ([82373a8](https://gitlab.com/hestia-earth/hestia-schema/commit/82373a862b3c4e9d36d4ca688ef95d38e081953c)), closes [#63](https://gitlab.com/hestia-earth/hestia-schema/issues/63)
* **organisation:** add `website` property ([55db072](https://gitlab.com/hestia-earth/hestia-schema/commit/55db07205a4adfeff9fed96859d1ecfa14e3b3e2))
* **organisation:** add uploadBy ([52267da](https://gitlab.com/hestia-earth/hestia-schema/commit/52267dafa3cf4c9efe80f3b3ea2d92d7b2f36286)), closes [#60](https://gitlab.com/hestia-earth/hestia-schema/issues/60)
* **source:** set uploadBy to required and simplify description ([e82f7fd](https://gitlab.com/hestia-earth/hestia-schema/commit/e82f7fdeaa887bba7f8f02b3fc0a9bc8da748c0f))

### [0.6.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.3...v0.6.4) (2021-01-21)


### Features

* **source:** add dataPrivate ([cd9f25a](https://gitlab.com/hestia-earth/hestia-schema/commit/cd9f25a68d352903ec86232db9f4225ca89263cc)), closes [#61](https://gitlab.com/hestia-earth/hestia-schema/issues/61)

### [0.6.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.2...v0.6.3) (2021-01-20)


### Features

* **term:** set `termType` as `required` ([9592f35](https://gitlab.com/hestia-earth/hestia-schema/commit/9592f3517fb7d40387212abaf318ed2364da6428))


### Bug Fixes

* **term:** remove `internal` on `termType` ([b5aba9d](https://gitlab.com/hestia-earth/hestia-schema/commit/b5aba9d9f001fc9718964d0644042f011171e870))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.1...v0.6.2) (2021-01-19)


### Features

* **json-schema:** export `definitions` type ([c28e330](https://gitlab.com/hestia-earth/hestia-schema/commit/c28e330b8dee86a884f70017a6cb88e2398989f7))


### Bug Fixes

* **source:** set `bibliography` to not `extend` ([d337a46](https://gitlab.com/hestia-earth/hestia-schema/commit/d337a46f63f320ddd1eed987bf7a5368557a29bc))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.0...v0.6.1) (2021-01-13)


### Features

* do not `extend` `dataPrivate` property ([f59c2c7](https://gitlab.com/hestia-earth/hestia-schema/commit/f59c2c7296c5e3e108cc861fa0e415d3ae34e1a8))
* **actor:** skip extend unused properties ([a3d377e](https://gitlab.com/hestia-earth/hestia-schema/commit/a3d377e92f546ce5814dc5535026d49ea8e456fa))
* **measurement:** restrict use of `termType` for textures ([fbcd6e5](https://gitlab.com/hestia-earth/hestia-schema/commit/fbcd6e50fb509a73a4267e190dd7947e1fc6c2a3))


### Bug Fixes

* **term:** set `termType` as `searchable` ([c8d12a8](https://gitlab.com/hestia-earth/hestia-schema/commit/c8d12a8f41a00bc5bf33abb6d56796388161026c))
* **term:** set `termType` to be extended ([3641a3b](https://gitlab.com/hestia-earth/hestia-schema/commit/3641a3bb08c283cda0e8e3ba235e8c6d1242e88f))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.5.0...v0.6.0) (2021-01-12)


### ⚠ BREAKING CHANGES

* **term:** `product.destination` no longer exists, along with `term.termType == 'destination'`
* **measurement:** `measurement.value` is now an `array` of `number`

### Features

* **measurement:** change value to array ([6cfba2f](https://gitlab.com/hestia-earth/hestia-schema/commit/6cfba2f7b7b024aee06efb21f9e43a1e2637d565))


### Bug Fixes

* **term:** remove `destination` as a `termType` ([deb8835](https://gitlab.com/hestia-earth/hestia-schema/commit/deb88354e6c521fe620730b898ac44b7d649e43e))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.8...v0.5.0) (2021-01-06)


### ⚠ BREAKING CHANGES

* **organisation:** `organisation.addressRegion` has been renamed to `organisation.region`

### Features

* **organisation:** change `addressRegion` to `region` as a reference to `Term` ([2c1f560](https://gitlab.com/hestia-earth/hestia-schema/commit/2c1f5607b43312e58904ea798a1cf2f44872f7e4))
* **term:** add manureManagement termType ([7d9708f](https://gitlab.com/hestia-earth/hestia-schema/commit/7d9708f7b1518c99a0f1461301dc1b7e2c4fbc10))

### [0.4.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.7...v0.4.8) (2020-12-29)


### Bug Fixes

* **schema:** fix `isExpandable` iterating over non-defined schemas ([8fb3e66](https://gitlab.com/hestia-earth/hestia-schema/commit/8fb3e668b8205479ead6ad095efc6da634bd3269))

### [0.4.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.6...v0.4.7) (2020-12-27)


### Features

* **cycle:** set `name` to `internal` and add `treatment` field ([67610a3](https://gitlab.com/hestia-earth/hestia-schema/commit/67610a318f9b4937c2a6b783817c21712a748b40)), closes [#51](https://gitlab.com/hestia-earth/hestia-schema/issues/51)

### [0.4.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.5...v0.4.6) (2020-12-21)


### Features

* **json schema:** add `integer` as generic type ([d2ea1ef](https://gitlab.com/hestia-earth/hestia-schema/commit/d2ea1ef0e846ecab1135f073f7937ea63199032d))
* **json-schema:** add const to definition ([21651a5](https://gitlab.com/hestia-earth/hestia-schema/commit/21651a58e3ab712e5262f126f66b8ee7d1bd6bfc))
* **product:** add pattern validation for ISO currency code ([20313d7](https://gitlab.com/hestia-earth/hestia-schema/commit/20313d70f3b42434b0fb5d984db43755d3862275))


### Bug Fixes

* **emission:** update format `startDate` and `endDate` ([6f66d4a](https://gitlab.com/hestia-earth/hestia-schema/commit/6f66d4a9cb963ce10206631696752a5360fb82e1))
* **types:** handle Date in `isExpandable` ([d64e274](https://gitlab.com/hestia-earth/hestia-schema/commit/d64e27408c77909078debdbff759899d296bd4ab))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.4...v0.4.5) (2020-12-15)


### Bug Fixes

* remove `enum` on `number` ([78c119a](https://gitlab.com/hestia-earth/hestia-schema/commit/78c119a299dcc36ae01feb119080aa9d2b9c6fb1))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.3...v0.4.4) (2020-12-08)


### Features

* **impact assessment:** add restrictions on list `termType` ([b3ca4fb](https://gitlab.com/hestia-earth/hestia-schema/commit/b3ca4fbc372727aa758fcbfc70a232fde7c51db6)), closes [#48](https://gitlab.com/hestia-earth/hestia-schema/issues/48)
* **term:** search by `units` ([5acc4cb](https://gitlab.com/hestia-earth/hestia-schema/commit/5acc4cb9cc0daa9de54044f2b1d3e291bbce3ab7))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.2...v0.4.3) (2020-12-02)


### Bug Fixes

* **json-schema:** handle default value as false ([18b1993](https://gitlab.com/hestia-earth/hestia-schema/commit/18b19931541a901c200777e112bd8f2c6cf0beb4))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.1...v0.4.2) (2020-11-29)


### Features

* restrict `termType` on all parent nodes ([018e383](https://gitlab.com/hestia-earth/hestia-schema/commit/018e3830e86a959b27140de38c3f1a6c712bcb6e))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.0...v0.4.1) (2020-11-27)


### Features

* **term:** add `system` to `termType` ([06071aa](https://gitlab.com/hestia-earth/hestia-schema/commit/06071aafcb2feb5015845787b18e9525ce38d8ed))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.3...v0.4.0) (2020-11-26)


### ⚠ BREAKING CHANGES

* **emission:** convert `emission.input` to `emission.inputs` as first element

### Features

* only set properties to not `extend` ([13e47ae](https://gitlab.com/hestia-earth/hestia-schema/commit/13e47aebbacfb1262113a1dec58ce0c05fbd15bf))
* **emission:** change `input` into a list as `inputs` ([99d3288](https://gitlab.com/hestia-earth/hestia-schema/commit/99d32886db0a45ea0e5bce030d4d2a6562279e62))
* **emission:** require `input` when using `methodTier = background` ([a1bd9e0](https://gitlab.com/hestia-earth/hestia-schema/commit/a1bd9e0851c8c249c8da2f7b5a04223ba3f7f0d9)), closes [#45](https://gitlab.com/hestia-earth/hestia-schema/issues/45)

### [0.3.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.2...v0.3.3) (2020-11-24)


### Bug Fixes

* **json-schema:** add missing `extend` on `[@id](https://gitlab.com/id)` and `[@type](https://gitlab.com/type)` ([dbe534b](https://gitlab.com/hestia-earth/hestia-schema/commit/dbe534b996bbd905753a744d8fba07d441c3e1d8))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.1...v0.3.2) (2020-11-24)


### Features

* add `extend` fields on every node ([c453263](https://gitlab.com/hestia-earth/hestia-schema/commit/c453263188f9c5fef7671746413625411980e622))
* add `glnNumber` field to `Site` and `Organisation` ([11596aa](https://gitlab.com/hestia-earth/hestia-schema/commit/11596aa28b4a1249f6b27d447d81b7a0771911f7))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.0...v0.3.1) (2020-11-15)


### Features

* **organisation:** set `country` as `required` ([74aed55](https://gitlab.com/hestia-earth/hestia-schema/commit/74aed557d717ce90f0004f6da3ce68511e03da98))
* **property:** add descriptive statistics ([f87c518](https://gitlab.com/hestia-earth/hestia-schema/commit/f87c518cd5a269aae907bd32a4a9bba568fb524f))


### Bug Fixes

* **measurement:** set `value` as non `required` ([32616f7](https://gitlab.com/hestia-earth/hestia-schema/commit/32616f74f5ea227e3af6c00a9aed8b9068d41e0b))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.2.0...v0.3.0) (2020-11-13)


### ⚠ BREAKING CHANGES

* **emission:** remove all references to `emission.chararacterisation`
* **term:** update `termType` = `characterisation` to `characterisedIndicator`

### Bug Fixes

* **emission:** delete chararacterisation field ([e8d1890](https://gitlab.com/hestia-earth/hestia-schema/commit/e8d189095305efe9e28ff051bc32ca9e75c2fd75))
* **term:** change characterisation to characterisedIndicator ([25454f6](https://gitlab.com/hestia-earth/hestia-schema/commit/25454f66498e368945b1064641e080fd84630611))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.9...v0.2.0) (2020-11-11)


### ⚠ BREAKING CHANGES

* **term:** use `animalManagement` instead of `dairyManagement` for termType
* **term:** use `other` instead of `seed` for `termType`
* **impact assessment:** every `Inventory` reference must be moved over to `ImpactAssessment` type
* **inventory:** all `Inventory` references will need migrating to `ImpactAssessment`

### Features

* add `originalId` to every node ([7e3121d](https://gitlab.com/hestia-earth/hestia-schema/commit/7e3121d750c49f6c4ad598478bf125af259979b6))
* add option for descriptive statistics to be `simulated` ([5e0469f](https://gitlab.com/hestia-earth/hestia-schema/commit/5e0469fa3b23d47dc3b7dc846d72ba6954604716))
* set `dataPrivate` to false by default ([0f8459f](https://gitlab.com/hestia-earth/hestia-schema/commit/0f8459f387afdcdd11f4ba7b05cdccbafefbc1dc))
* **emission:** add `input` field ([3fab660](https://gitlab.com/hestia-earth/hestia-schema/commit/3fab660609c67d471ddb2d765b33d9ebe8e99385))
* **impact assessment:** add `country` and `region` as fields ([5122e26](https://gitlab.com/hestia-earth/hestia-schema/commit/5122e2682ec71641cc77b8a921023253c10900b6))
* **impact assessment:** convert from `inventory` ([124e6ab](https://gitlab.com/hestia-earth/hestia-schema/commit/124e6abb61437a0718cf73f3dd20fae51a798b1b))
* **site:** add `not specified` siteType ([e7838a3](https://gitlab.com/hestia-earth/hestia-schema/commit/e7838a3f8d4144616e63c2a933dc51734a9d8a35))
* **source:** add `unique` on `bibliography` and `bibliography.title` ([fbb553b](https://gitlab.com/hestia-earth/hestia-schema/commit/fbb553babe3b8e9de3ec756c4ac90ca27e1eb4ee))
* **term:** add `transport` termType ([ed42d96](https://gitlab.com/hestia-earth/hestia-schema/commit/ed42d962ea45234ac2678326bc5c6745c0121cd3))


### Bug Fixes

* **infrastructure:** change `dataVersion` to `string` ([ead46af](https://gitlab.com/hestia-earth/hestia-schema/commit/ead46af466a437288df1c47a9f079b903f3dea57))
* **site:** fix referenced node for `practices` ([1bd1663](https://gitlab.com/hestia-earth/hestia-schema/commit/1bd1663608156df8ab1aadb26d5cf40d7e1811b5))
* **site:** remove unused `dataState` and `dataVersion` ([1a68815](https://gitlab.com/hestia-earth/hestia-schema/commit/1a688154595f3180ac204ac4cc7d0a78ffa3b014))
* **source:** remove `design` ([29287e7](https://gitlab.com/hestia-earth/hestia-schema/commit/29287e753873707a3ab14b50710a040917aafbfc))
* **term:** change `seed` termType to `other` ([46f533d](https://gitlab.com/hestia-earth/hestia-schema/commit/46f533d1470ce218c8a452c0b061fdbacf53b81d))
* **term:** replace `dairyManagement` with `animalManagement` termType ([ed32ed5](https://gitlab.com/hestia-earth/hestia-schema/commit/ed32ed5c41e9715a55c0a2230953dff34b38d06c))


* **inventory:** change name to `impact assessment` ([3add6a7](https://gitlab.com/hestia-earth/hestia-schema/commit/3add6a7de93fb05df8025789bf878b37904c9f32))

### [0.1.9](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.8...v0.1.9) (2020-11-04)


### Features

* **term:** add `searchable` to `synonyms` ([ce4740a](https://gitlab.com/hestia-earth/hestia-schema/commit/ce4740a5c3ea279eab64fe596fda20b7e8ced12f))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.7...v0.1.8) (2020-11-01)


### Features

* **inventory:** make `cycle`, `product` and `source` searchable ([4efa8fb](https://gitlab.com/hestia-earth/hestia-schema/commit/4efa8fbf0e2f9d184a751e795092d3b38b844f92)), closes [#33](https://gitlab.com/hestia-earth/hestia-schema/issues/33)
* **inventory:** set `source` as `required` ([020489e](https://gitlab.com/hestia-earth/hestia-schema/commit/020489efd5c51b8256c20f837df9ac09b030180a))
* **term:** add `method` to `termType` ([05616b6](https://gitlab.com/hestia-earth/hestia-schema/commit/05616b6f51ca852bc8b07fc23e94477b83e45d6b))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.6...v0.1.7) (2020-10-24)


### Bug Fixes

* **product:** set `value` as not `required` ([a3e4a69](https://gitlab.com/hestia-earth/hestia-schema/commit/a3e4a698c64d05c3d3688db1a7fc9d6c22cc694e))
* **term:** remove `cropProduct` from `termType` ([d1d752f](https://gitlab.com/hestia-earth/hestia-schema/commit/d1d752f6d743b9bf152769f1422a9cfdc603f3cd))
* **term:** remove unused `dependentVariables` and `independentVariables` ([a141ce1](https://gitlab.com/hestia-earth/hestia-schema/commit/a141ce1be5ff7ea6eb978c0a81daf01d685870fd))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.5...v0.1.6) (2020-10-22)


### Features

* **inventory:** add source ([09183a6](https://gitlab.com/hestia-earth/hestia-schema/commit/09183a656c6eed1c13738f9629ea21d3c91ba115))
* **inventory:** clarify terms, add midpoint ([76a6bcf](https://gitlab.com/hestia-earth/hestia-schema/commit/76a6bcf2a49411f6a7f69275fe73d9daf5eb72ed)), closes [#29](https://gitlab.com/hestia-earth/hestia-schema/issues/29)

### [0.1.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.4...v0.1.5) (2020-10-17)


### Features

* **product:** add primary product identifier ([3b540ff](https://gitlab.com/hestia-earth/hestia-schema/commit/3b540ff6cd39468efd5700a29a01529e4a486bdc))
* **site:** add ecoregion ([29618d1](https://gitlab.com/hestia-earth/hestia-schema/commit/29618d197d6880027aec6310fad15936d98e77c2)), closes [#30](https://gitlab.com/hestia-earth/hestia-schema/issues/30)

### [0.1.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.3...v0.1.4) (2020-09-29)


### Features

* **actor:** set name to internal ([48fa6ff](https://gitlab.com/hestia-earth/hestia-schema/commit/48fa6ffd9f1881a462b42fb6d7a7e85a352c813c))
* **bibliography:** change type of issue to string ([3f4eafd](https://gitlab.com/hestia-earth/hestia-schema/commit/3f4eafd199dad2f40574e4b20e85128f11e2ccc6)), closes [#27](https://gitlab.com/hestia-earth/hestia-schema/issues/27)
* **source:** set metaAnalysisBibliography to searchable ([bc1a963](https://gitlab.com/hestia-earth/hestia-schema/commit/bc1a963d406c0943fac9308001f21d96ef0f8764))
* **validate jsonld:** handle validate file for upload ([a732467](https://gitlab.com/hestia-earth/hestia-schema/commit/a73246774dc73f12a33b99895133d08ed95471f3))


### Bug Fixes

* **excel template:** remove subRegion ([7916991](https://gitlab.com/hestia-earth/hestia-schema/commit/7916991fd90718b63788b337d00fcc4797ce61c3))
* **json-schema:** generate deep version only for top nodes ([f485862](https://gitlab.com/hestia-earth/hestia-schema/commit/f48586246a5b9dcd3f7050790ee4d98f9d4e15f2))
* **source:** set name and bibliography to required ([30a1b73](https://gitlab.com/hestia-earth/hestia-schema/commit/30a1b7306ed6cf6f081cc9af2d0bf3d97ada5142))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.2...v0.1.3) (2020-09-07)


### Features

* **bibliography:** add `name` back ([d400049](https://gitlab.com/hestia-earth/hestia-schema/commit/d400049f730d58160c92e5302c08ad40ca0a758a))
* **cycle:** change `alternativeStartDate` to `altStartDate` ([0fc600c](https://gitlab.com/hestia-earth/hestia-schema/commit/0fc600ce54c54b12f6363ea0c1b36beaf08ea493))
* **excel template:** convert all dates to ISO format ([7ab57f7](https://gitlab.com/hestia-earth/hestia-schema/commit/7ab57f7eb9f6badc088e0b8ae4683afc3eaba1b0))
* **site:** merge subRegions into `region` ([4f807f8](https://gitlab.com/hestia-earth/hestia-schema/commit/4f807f8e99459b90fc70579052b4c298b889beda)), closes [#25](https://gitlab.com/hestia-earth/hestia-schema/issues/25)
* **term:** add `termType` `standardsLabels` ([6f4ac18](https://gitlab.com/hestia-earth/hestia-schema/commit/6f4ac18834bc790b41e5feece5fcf3c352930030))
* **term:** add ecolabel as option ([575e0ae](https://gitlab.com/hestia-earth/hestia-schema/commit/575e0aec572a0abaa2663dc9881bb8059c6fb079))


### Bug Fixes

* make date formats consistent ([fe1d9c3](https://gitlab.com/hestia-earth/hestia-schema/commit/fe1d9c31cea656a05be845df37574bece73d4716)), closes [#26](https://gitlab.com/hestia-earth/hestia-schema/issues/26)
* **bibliography:** set `dateAccessed` as a list of dates ([c9a5644](https://gitlab.com/hestia-earth/hestia-schema/commit/c9a564459ac1f8550dcb6f30d1a70649da475504))
* **measurement:** merge time and date and fix ISO example ([3e9f97d](https://gitlab.com/hestia-earth/hestia-schema/commit/3e9f97d69383fec5d6174ee3b499f3d684e051d0))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.1...v0.1.2) (2020-09-02)


### Bug Fixes

* replace dataVersions by dataVersion ([b212873](https://gitlab.com/hestia-earth/hestia-schema/commit/b21287374ec5f10f23e2fafb1dfc7e38b158cba0))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.0...v0.1.1) (2020-09-01)


### Bug Fixes

* **validate jsonld:** handle .hestia files ([c74e5a8](https://gitlab.com/hestia-earth/hestia-schema/commit/c74e5a83ae12651474d94092caae15e01871c87a))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.29...v0.1.0) (2020-09-01)


### Bug Fixes

* **validate jsonld:** handle both json/jsonld extensions ([fc0c9c8](https://gitlab.com/hestia-earth/hestia-schema/commit/fc0c9c804c5f62f35cd5490c079b8f06fb00fe3a))

### [0.0.29](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.28...v0.0.29) (2020-09-01)


### Features

* change dataVersions to string ([d129823](https://gitlab.com/hestia-earth/hestia-schema/commit/d129823cc5c7b070e9c06e860b1a4a9d75f2de40))
* **bibliography:** add required fields ([3cc26ec](https://gitlab.com/hestia-earth/hestia-schema/commit/3cc26ec7fa905d43475b649727420f56cc77f781))
* **bibliography:** make a blank node ([4a65079](https://gitlab.com/hestia-earth/hestia-schema/commit/4a65079d6097f24c9d7e3bb2bdf3188bf1587482)), closes [#23](https://gitlab.com/hestia-earth/hestia-schema/issues/23)
* **cycle:** make cycle.site mandatory ([39bfc6f](https://gitlab.com/hestia-earth/hestia-schema/commit/39bfc6f1555e2d28b70a87571ede657f718137f6))
* **practice:** add startDate and endDate ([e09fad2](https://gitlab.com/hestia-earth/hestia-schema/commit/e09fad2b465734cc860290a788fa7a4d2d23a82f))
* **term:** add `termType`: `soilTexture` ([082ef81](https://gitlab.com/hestia-earth/hestia-schema/commit/082ef81791babe5149afdc26111174816908407a))


### Bug Fixes

* **bibliography:** remove low importance fields ([389e8b4](https://gitlab.com/hestia-earth/hestia-schema/commit/389e8b4b770a19c7e20c44fc27e814578cc923ea))
* **practice:** make stats definitions consistent ([43e71d1](https://gitlab.com/hestia-earth/hestia-schema/commit/43e71d14579810109d29f163bea943fa4d9e0070))

### [0.0.28](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.27...v0.0.28) (2020-08-31)


### Features

* **actor:** add searchable fields ([da51839](https://gitlab.com/hestia-earth/hestia-schema/commit/da518391f0d13dce7a4265beb3aa1fa56db15259))
* **bibliography:** add more searchable fields ([dbdf974](https://gitlab.com/hestia-earth/hestia-schema/commit/dbdf9744a76a10414e23dbd7e67ac88b0a3c313b))
* **site:** add searchable on siteType and defaultSource ([7883815](https://gitlab.com/hestia-earth/hestia-schema/commit/7883815dbf313a48a2d0a742d01e2f5cc6c066a9))

### [0.0.27](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.26...v0.0.27) (2020-08-25)


### Features

* **cycle:** add alternativeStartDate field ([2ce5095](https://gitlab.com/hestia-earth/hestia-schema/commit/2ce5095ba69be2c573695ea392669578549d030d))
* **cycle:** add alternativeStartDateDefinition field ([2bbfd16](https://gitlab.com/hestia-earth/hestia-schema/commit/2bbfd1673fcd82d786a3eef45a7dd0bd88b610e7))
* **cycle:** remove treatment and treatmentDescription ([9bf7eda](https://gitlab.com/hestia-earth/hestia-schema/commit/9bf7eda22561ee78155a33f74387394379375491))
* **infrastructure:** add area ([8bbc244](https://gitlab.com/hestia-earth/hestia-schema/commit/8bbc244efaf15c6228d88521643065122e5274ac))
* **infrastructure:** add hours for lifespan and mass ([963377d](https://gitlab.com/hestia-earth/hestia-schema/commit/963377d896b674eb6f95fe9eddbd803089e1aa25))
* **measurement:** replace date with startDate + endDate ([96c47d9](https://gitlab.com/hestia-earth/hestia-schema/commit/96c47d96598c610ddf58ae3f4ae1516dff56a65d))
* **practice:** add sources and dataState ([494292f](https://gitlab.com/hestia-earth/hestia-schema/commit/494292fb71c9795a3a14045a3e03deedbe79e1b2))


### Bug Fixes

* **source:** remove internal from uploadNotes field ([cc0cb4b](https://gitlab.com/hestia-earth/hestia-schema/commit/cc0cb4b6ba20bafaf68f68bfbcda144a20224817))

### [0.0.26](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.25...v0.0.26) (2020-08-25)


### Features

* export version as SCHEMA_VERSION ([0a5c419](https://gitlab.com/hestia-earth/hestia-schema/commit/0a5c41921bb5887b67447313bf956769547b171a))


### Bug Fixes

* **json-schema:** exclude some properties from default values ([48b8c31](https://gitlab.com/hestia-earth/hestia-schema/commit/48b8c313c6995c7c56a2ca169d6dd8a5f832f19e))

### [0.0.25](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.24...v0.0.25) (2020-08-21)


### Features

* add searchable on all valuable terms ([5b778c2](https://gitlab.com/hestia-earth/hestia-schema/commit/5b778c21270114f4082f0b43a6995abb919076cd))

### [0.0.24](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.23...v0.0.24) (2020-08-21)


### Features

* **cycle:** add searchable on inputs, emissions, products and practices ([a3449dc](https://gitlab.com/hestia-earth/hestia-schema/commit/a3449dc9f7b0698755b9d1e38d072d0e2e7b13e6))
* **property:** add dataState and dataVersions ([40e2da0](https://gitlab.com/hestia-earth/hestia-schema/commit/40e2da04ebd3d6a0bf6987cbcd84423db476fc40))

### [0.0.23](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.22...v0.0.23) (2020-08-19)


### Bug Fixes

* **term:** set name as searchable ([84353f7](https://gitlab.com/hestia-earth/hestia-schema/commit/84353f784bb8b92d5f4825861d5ba2aa65884c8d))

### [0.0.22](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.21...v0.0.22) (2020-08-19)


### Features

* change how methodTier is defined ([ac30986](https://gitlab.com/hestia-earth/hestia-schema/commit/ac309864561d998e4920f382219b58e371776a42))
* **json-schema:** add function get default properties ([08014a0](https://gitlab.com/hestia-earth/hestia-schema/commit/08014a0bf6cab9160ead1133b4b3448fc3c4ba24))
* **site:** improve definition of siteType field ([4b278e4](https://gitlab.com/hestia-earth/hestia-schema/commit/4b278e45f688878e6e3daadcf1084b9bb9cff3dc))
* **site:** make siteType a required field ([131cf2d](https://gitlab.com/hestia-earth/hestia-schema/commit/131cf2df70dbfd9611f8d255c1d3e756c98f72b7))


### Bug Fixes

* improve clarity of dataState and dataVersions fields; add default: original to dataState ([1cb313e](https://gitlab.com/hestia-earth/hestia-schema/commit/1cb313e8b16458f6508ebc8af054495738fdf64b))
* **excel_template:** add dropdown for siteType ([13376cf](https://gitlab.com/hestia-earth/hestia-schema/commit/13376cfd43a5ac6ccc75605f47a10f5dff0c5d41))
* **inventory:** update example ([1143161](https://gitlab.com/hestia-earth/hestia-schema/commit/1143161b211e781cb336f0df6b61bf28408dd7ba))

### [0.0.21](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.20...v0.0.21) (2020-08-17)


### Features

* **source:** search by bibliography ([506e307](https://gitlab.com/hestia-earth/hestia-schema/commit/506e3072b8754d6ead0c2854faa71ee1974b1adc))
* **types:** export searchable properties ([7d392bc](https://gitlab.com/hestia-earth/hestia-schema/commit/7d392bccddfdc1c2fdc0b318bd5c466d5d6f2378))


### Bug Fixes

* **cycle:** set Site as searchable ([36f48b0](https://gitlab.com/hestia-earth/hestia-schema/commit/36f48b0ebb2232bc12126c38ae5bbed1ccd6aca8))

### [0.0.20](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.19...v0.0.20) (2020-08-16)


### Bug Fixes

* fix module not found outside node env ([fa4d01a](https://gitlab.com/hestia-earth/hestia-schema/commit/fa4d01a0583649f84b4caaa3872a1ac837ea5802))

### [0.0.19](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.18...v0.0.19) (2020-08-16)


### Features

* add min and max as well as sd ([2d9dc34](https://gitlab.com/hestia-earth/hestia-schema/commit/2d9dc340c2614ce0695c6b81ed612d889fc4e2a2)), closes [#18](https://gitlab.com/hestia-earth/hestia-schema/issues/18)
* add option to declare that multiple Cycles or Sites were aggregated ([e6e61e2](https://gitlab.com/hestia-earth/hestia-schema/commit/e6e61e2c8066980e32b48a88b945361985fadd87))
* add reliability assessment ([7ab2a21](https://gitlab.com/hestia-earth/hestia-schema/commit/7ab2a21ebf4860b3fda6e41068a80ee6254f24fa)), closes [#18](https://gitlab.com/hestia-earth/hestia-schema/issues/18)
* **practice:** enable properties to be added to practices ([bd83360](https://gitlab.com/hestia-earth/hestia-schema/commit/bd833601afff920eb84c80796e88eb4f2aadbd20))
* **product:** add dataState and dataVersion ([99f7fb8](https://gitlab.com/hestia-earth/hestia-schema/commit/99f7fb82167e9ad43c106cb2fec7f9288838edfe))
* **source:** add uploadNotes free text field to describe data upload ([3e417b6](https://gitlab.com/hestia-earth/hestia-schema/commit/3e417b6cb7c08862d52775c07a252d2006fbf1eb))
* **term:** add usdaSoilType ([78ab171](https://gitlab.com/hestia-earth/hestia-schema/commit/78ab1711f8c6be446bec95c105b4b47a20e2a28b)), closes [#32](https://gitlab.com/hestia-earth/hestia-schema/issues/32)


### Bug Fixes

* remove recalculated field ([0046591](https://gitlab.com/hestia-earth/hestia-schema/commit/00465914bff456dcdfaeec2da85727125fdebefe))
* update examples for statsDefinition field ([5e5c973](https://gitlab.com/hestia-earth/hestia-schema/commit/5e5c973bdc97daec890f6107d92aedd276676b40))
* **cycle:** clarify functional unit definition ([a978606](https://gitlab.com/hestia-earth/hestia-schema/commit/a9786065d015e835e6fbf0e87430605baedf72f8))
* **infrastructure:** clarify description ([a03e240](https://gitlab.com/hestia-earth/hestia-schema/commit/a03e240a1dd2414cfb28a43d749f47497b3bc419))
* **measurement:** better define units ([1cc525d](https://gitlab.com/hestia-earth/hestia-schema/commit/1cc525db008663dd70ed5cb53a9c37417b3bff6c))
* **organisation:** lat and lon definitions ([b5d17a9](https://gitlab.com/hestia-earth/hestia-schema/commit/b5d17a9bba84d572990b83c43cd68904b6c6d07f))
* **site:** add sdDefinition ([fb2dd88](https://gitlab.com/hestia-earth/hestia-schema/commit/fb2dd88cf6a846e2a0a971e410b5450348f7850c))
* **site:** fix longitude description ([66b48b5](https://gitlab.com/hestia-earth/hestia-schema/commit/66b48b5be992f0bece849d92c3761c37cfb497f9))
* **site:** improve field definitions ([e81f9dc](https://gitlab.com/hestia-earth/hestia-schema/commit/e81f9dcf3cb9a584dc7e4b310e54dd1ce7ae7d13))
* **site:** typo in searchable key ([ca4fcd4](https://gitlab.com/hestia-earth/hestia-schema/commit/ca4fcd41c414974e66d7150c092f91fafc487c52))
* **term:** change termType from required: true to internal: true ([30c8760](https://gitlab.com/hestia-earth/hestia-schema/commit/30c8760b498547938a7fd1454df9a20f5e348702))

### [0.0.18](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.17...v0.0.18) (2020-08-13)

### [0.0.17](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.16...v0.0.17) (2020-08-08)


### Features

* **json-schema:** add javascript helper load all schemas ([16e0e0b](https://gitlab.com/hestia-earth/hestia-schema/commit/16e0e0b4e46ac03f1ec3c8cf8658ad6e748655ab))

### [0.0.16](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.15...v0.0.16) (2020-08-07)


### Features

* add searchable key ([fe8bac1](https://gitlab.com/hestia-earth/hestia-schema/commit/fe8bac159ddbc113f92f8c8f03bda1dfdc1ec3ed))


### Bug Fixes

* **term:** remove deprecated shortName property ([9303b33](https://gitlab.com/hestia-earth/hestia-schema/commit/9303b33946066e6cc15488040b12332fc03a5ba3))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.14...v0.0.15) (2020-08-04)


### Features

* make dataPrivate field required ([a28ebf6](https://gitlab.com/hestia-earth/hestia-schema/commit/a28ebf61d3af2ce3ac51d580cf068ccca499c510))
* **practices:** move to Practice / List[Practice] ([f8d9b48](https://gitlab.com/hestia-earth/hestia-schema/commit/f8d9b48e87fddb1e87d588811aa5e1d90f8545f8))
* **site:** add aquaculture pens as a siteType ([9aabf1f](https://gitlab.com/hestia-earth/hestia-schema/commit/9aabf1f21ad7aa7547af94f33f6a53d0c89bae26))


### Bug Fixes

* change farm to organisation in sdDefinition ([3bc8578](https://gitlab.com/hestia-earth/hestia-schema/commit/3bc8578dd6b094cac1a5ba406e0294bc6765dc5e))
* move dataState and dataVesions into the blank nodes ([81c39d9](https://gitlab.com/hestia-earth/hestia-schema/commit/81c39d902366427afa1b5f755adc8ae39f224621))
* **cycle:** remove dataState ([5bd2f1b](https://gitlab.com/hestia-earth/hestia-schema/commit/5bd2f1bdc2b7fa53ce0a989b363a2aacb2435e1b))
* **organisation:** remove unique and required from name ([c104552](https://gitlab.com/hestia-earth/hestia-schema/commit/c1045526eff292d4e40e14377d4edeaf3b181761))
* **practices:** follow Input/Property schema ([d48b086](https://gitlab.com/hestia-earth/hestia-schema/commit/d48b086270741e459a0ec76cac067aaf7be08d07))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.13...v0.0.14) (2020-07-23)


### Features

* **python:** add JSONLD classes ([60dd58e](https://gitlab.com/hestia-earth/hestia-schema/commit/60dd58e0d94c40c3bf57003e05f6c4ddc8cc631f))
* **python:** add to_dict method to filter out non-required empty values ([8c8778b](https://gitlab.com/hestia-earth/hestia-schema/commit/8c8778b7635e08332754420b5faf0d069aa60b68))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.12...v0.0.13) (2020-07-16)


### Features

* change country from string to Ref[Term] ([a33660b](https://gitlab.com/hestia-earth/hestia-schema/commit/a33660bf695e074d205e9f32570db543d25732a0))
* **term:** add cropProduct ([8809f30](https://gitlab.com/hestia-earth/hestia-schema/commit/8809f30ef7a77c78a72eda5c67fd2705eddb3028))
* **terms:** mark shortName as deprecated ([08acfbb](https://gitlab.com/hestia-earth/hestia-schema/commit/08acfbb4cc0abd44bdb44dfcc6f88548ffb44c64))


### Bug Fixes

* **bibliography:** typo ([4cbf69d](https://gitlab.com/hestia-earth/hestia-schema/commit/4cbf69d5b244047cf27d9ae6c5d9a0aaff2c8a20))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.11...v0.0.12) (2020-07-16)


### Bug Fixes

* normalize country to use Term ([3ab1d5e](https://gitlab.com/hestia-earth/hestia-schema/commit/3ab1d5e30d4cc8dbd050272b1f238e7af574a56d))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.10...v0.0.11) (2020-07-16)

### [0.0.10](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.9...v0.0.10) (2020-07-16)

### [0.0.9](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.8...v0.0.9) (2020-07-16)

### [0.0.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.7...v0.0.8) (2020-07-16)


### Features

* move practices into blank node ([7372c88](https://gitlab.com/hestia-earth/hestia-schema/commit/7372c88bf47d3bf0e0eaaa665bda88a595a5bced))
* **cycle:** add further fields ([9225439](https://gitlab.com/hestia-earth/hestia-schema/commit/9225439fafd35b0275e46d03c5b9ffb693f6a195))
* **cycles:** change Completeness and Practices so single Embed ([b83d7d2](https://gitlab.com/hestia-earth/hestia-schema/commit/b83d7d2c4744c579be67d01568ad1cd2e05b1494))
* **organisation:** add area and boundary for farms ([190c850](https://gitlab.com/hestia-earth/hestia-schema/commit/190c85093c95e128efe3c68056fd097513faa610))
* **site:** differentiate siteType into cropland and pasture ([51b42f0](https://gitlab.com/hestia-earth/hestia-schema/commit/51b42f0c0cdf63503bf8f154300b884b626c8294))
* **term:** add cropEstablishment term ([c0f3d6d](https://gitlab.com/hestia-earth/hestia-schema/commit/c0f3d6d93972d598607e0dfc5d93754bc024328e))


### Bug Fixes

* change term order, set name to internal ([7cda43c](https://gitlab.com/hestia-earth/hestia-schema/commit/7cda43ca0776e7139699a5d8b6c7a5e24b6ae215))
* **completeness:** add compost to definition ([b2045c6](https://gitlab.com/hestia-earth/hestia-schema/commit/b2045c62d3cec94f6c200331471ea966735758da))
* **completeness:** field names for readibility ([a56f41b](https://gitlab.com/hestia-earth/hestia-schema/commit/a56f41b00eac8642b581e9b2e6b79bc97b856a9a))
* **input:** remove required on value ([567da80](https://gitlab.com/hestia-earth/hestia-schema/commit/567da807387a79a460a2f13d9ca433f003bd3019))
* **inventory:** rename field ([ff56474](https://gitlab.com/hestia-earth/hestia-schema/commit/ff564741ce268cf69f143dec67749fb5be4ddbbb))
* **measurement:** change definition of sd ([5393ec0](https://gitlab.com/hestia-earth/hestia-schema/commit/5393ec08363b198ae4a22ccd36bc503e65ab9e64))
* **practices:** correct typos ([0a799b2](https://gitlab.com/hestia-earth/hestia-schema/commit/0a799b2c8a93d216542ebecc3105c46a5fac9bc3))
* **site:** typo ([4b59cd0](https://gitlab.com/hestia-earth/hestia-schema/commit/4b59cd05f82e8e369795066c202ba9400a703de1))
* **source:** allow custom names for sources ([1931192](https://gitlab.com/hestia-earth/hestia-schema/commit/19311921744921015e14b848d3d52a5d5b094763))
* **source:** change uploadBy and validationBy to internal properties ([b6bd3b1](https://gitlab.com/hestia-earth/hestia-schema/commit/b6bd3b1600c8856bd52f39a43f5c3f7b64269cdd))
* **term:** change termType floodingRegime to waterRegime ([4684098](https://gitlab.com/hestia-earth/hestia-schema/commit/468409895927ba48f3c344790c93a731198eea84))
* **types:** allow NodeType when generating jsonldPath ([b321a1f](https://gitlab.com/hestia-earth/hestia-schema/commit/b321a1fbfa7d6f03ab8b1793006f14ce66111458))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.6...v0.0.7) (2020-07-15)


### Features

* add further required terms ([62b9add](https://gitlab.com/hestia-earth/hestia-schema/commit/62b9add6b6b9cd4a0c9b61035e74663dea5d8ea5))
* set some properties as internal ([03dfdc7](https://gitlab.com/hestia-earth/hestia-schema/commit/03dfdc70b5d4e20ff166aa079467f405135525c9))
* **actor:** add dataPrivate field ([346195e](https://gitlab.com/hestia-earth/hestia-schema/commit/346195ed9d4b5471276008b67b5787634a065731))
* **cycles:** add description, dataState and dataVersions properties ([ef293c6](https://gitlab.com/hestia-earth/hestia-schema/commit/ef293c686115f74ef3c0ef96a653c6bfd7fbd693))
* **emissions:** add startDate, endDate and emissionDuration properties ([8e5b8e2](https://gitlab.com/hestia-earth/hestia-schema/commit/8e5b8e216f4bc33d3cf5c494ac8a0014d78a3d3b))
* **inventories:** add dataState and dataVersions properties ([aee1418](https://gitlab.com/hestia-earth/hestia-schema/commit/aee1418a8849ab695d491bbcbf8a8b12a9ad88a1))
* **sites:** add description, dataState and dataVersions properties ([04e2476](https://gitlab.com/hestia-earth/hestia-schema/commit/04e2476b9573bfde2a9fafbdaf01e7b0af8f3205))
* **terms:** add required shortName ([1466edb](https://gitlab.com/hestia-earth/hestia-schema/commit/1466edbf941816fff267faec36cc46f594210635))


### Bug Fixes

* **terms:** remove required shortName ([db8616e](https://gitlab.com/hestia-earth/hestia-schema/commit/db8616ee1389fa2cf4b167c30a86d22930e332f6))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.5...v0.0.6) (2020-07-07)


### Features

* add required fields ([b0fd69c](https://gitlab.com/hestia-earth/hestia-schema/commit/b0fd69c42644ceb64e37267a0bddcc4811ae3ead))
* add unique fields ([316f22d](https://gitlab.com/hestia-earth/hestia-schema/commit/316f22d11df551f51a069b356b53d3f50053cba2))
* **sites:** add extra GADM layer level 5 ([0e4f675](https://gitlab.com/hestia-earth/hestia-schema/commit/0e4f6759c202c2ed21438ced81efd159a8b61b3f))


### Bug Fixes

* **terms:** add resourceUse termType ([1bdaa55](https://gitlab.com/hestia-earth/hestia-schema/commit/1bdaa55fa76a8695d0ee8fbd397b20af763cf2ea)), closes [#9](https://gitlab.com/hestia-earth/hestia-schema/issues/9)

### [0.0.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.4...v0.0.5) (2020-06-29)


### Features

* add `recalculated` field on Emission, Input and Product ([a26d040](https://gitlab.com/hestia-earth/hestia-schema/commit/a26d040675191a17ebb6df05c10f7095d8150e24))
* handle array[number] type ([e1cce60](https://gitlab.com/hestia-earth/hestia-schema/commit/e1cce60ffa704f879637d260213dc7fd48ca3e22)), closes [#5](https://gitlab.com/hestia-earth/hestia-schema/issues/5)
* **property:** add valueType to determine parsing on value ([c86228e](https://gitlab.com/hestia-earth/hestia-schema/commit/c86228e215b9d96a9a56535a22ff70e848ba6c52)), closes [#4](https://gitlab.com/hestia-earth/hestia-schema/issues/4)
* **sites:** use GADM regions as Term ([f92ff11](https://gitlab.com/hestia-earth/hestia-schema/commit/f92ff11821a4f0599cc3601c300efeafacea6d2e))
* **terms:** add termTypes ecoregion, region and liveAquaticSpecies ([46fef2b](https://gitlab.com/hestia-earth/hestia-schema/commit/46fef2b5d077c5f0b2ebc9504ee3394a2479ca7a))


### Bug Fixes

* **completeness:** change `coProducts` to `products` and `residue` to `cropRes` ([fd35f82](https://gitlab.com/hestia-earth/hestia-schema/commit/fd35f82531fcf3558b6f632874f6450486777f6c))
* **generate json-schemas:** handle arrays of types ([c9c6dcd](https://gitlab.com/hestia-earth/hestia-schema/commit/c9c6dcd4f34a2f458aa57ac06da4f1088c060c5d))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.3...v0.0.4) (2020-06-15)


### Features

* **emissions:** add characterisation property ([d8cdc3a](https://gitlab.com/hestia-earth/hestia-schema/commit/d8cdc3a82680643a72b5edd5347eff0ee3d8da6e))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.2...v0.0.3) (2020-05-18)


### Bug Fixes

* **enums:** fix wrong enum values ([d674da9](https://gitlab.com/hestia-earth/hestia-schema/commit/d674da9f163af983c29ff99e91eb3f26f56dbe36))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.1...v0.0.2) (2020-05-17)


### Bug Fixes

* **generate:** handle list for python ([4b7d2c5](https://gitlab.com/hestia-earth/hestia-schema/commit/4b7d2c5ee884c6c0d9335779c0565a700fad9435))

### 0.0.1 (2020-05-15)


### Features

* update content and examples ([984eee2](https://gitlab.com/hestia-earth/hestia-schema/commit/984eee2bcffd20efd127083c2a99eaede7d54794))
* **context:** generate context files for each node ([6cf79d7](https://gitlab.com/hestia-earth/hestia-schema/commit/6cf79d7db44ada66a4429873cf261daf8046bda4))
* **context:** handle GeoJSON type ([f04a92b](https://gitlab.com/hestia-earth/hestia-schema/commit/f04a92bac4a2117b9a6fde5cf6ce8da7cfac9931))
* **generate types:** generate enum ([62289cc](https://gitlab.com/hestia-earth/hestia-schema/commit/62289ccac25384cd6b5ddb2b7211cb6a6835a27b))
* **index:** update link to glossary ([f24dc0e](https://gitlab.com/hestia-earth/hestia-schema/commit/f24dc0e86eea53013e2a559acda7994610791243))
* **json-schema:** add generated files ([00adc55](https://gitlab.com/hestia-earth/hestia-schema/commit/00adc559468dae7038124d82b5b06315267a00e2))
* **json-schema:** generate from yaml files ([bf8a4fd](https://gitlab.com/hestia-earth/hestia-schema/commit/bf8a4fd6af63cbbe4ce3aaf13ffdd095758269e4))
* **schema:** add enum and function for type Node ([f8a22d8](https://gitlab.com/hestia-earth/hestia-schema/commit/f8a22d801e1f6fa0c18c52aac5f2c3613b2d6de9))
* **schema:** add enum values to generated types ([a5c9d1e](https://gitlab.com/hestia-earth/hestia-schema/commit/a5c9d1e51e7c9a344a69a305d10e376fe3a8a0e5))
* **schema:** add list of content types ([d5dc31d](https://gitlab.com/hestia-earth/hestia-schema/commit/d5dc31de1708c21bca24c3255a0b12a0fd7ab247))
* **schema:** generate typescript classes from yaml files ([36db121](https://gitlab.com/hestia-earth/hestia-schema/commit/36db121d0828f9bbbe00c8aff8375381e8f2b4df))
* **schema:** update types ([ca04f94](https://gitlab.com/hestia-earth/hestia-schema/commit/ca04f94ce821b08e78836815d17e34c759e021f4))
* **templates:** add json-ld views ([da91027](https://gitlab.com/hestia-earth/hestia-schema/commit/da91027897b330b2918deb44f8e1e47c92931b6b))
* **templates:** add tooltip on copy button ([6376d78](https://gitlab.com/hestia-earth/hestia-schema/commit/6376d78fb8464cfc2ef45b135b4d4ee908eb0207))
* **templates:** include navbar and sidebar ([ed6c0c5](https://gitlab.com/hestia-earth/hestia-schema/commit/ed6c0c56bd519442439b8d47465a4c8f043e4a55))
* **templates:** update class page title ([456513a](https://gitlab.com/hestia-earth/hestia-schema/commit/456513aeb9b237e67786a3817f9a208214b8d581))
* **templates:** update container styles ([f26da0b](https://gitlab.com/hestia-earth/hestia-schema/commit/f26da0b1b10e43b432e82fb4406e6be14d52fe1b))
* **templates:** update title for index page ([ee459d9](https://gitlab.com/hestia-earth/hestia-schema/commit/ee459d938c5662db7952919a7c560a28d263a11d))
* **templates:** use highlight + copy for all codes ([ae4e026](https://gitlab.com/hestia-earth/hestia-schema/commit/ae4e026d4be88a6baa2b0ca97f2563f64d31c421))
* **templates index:** add basis for content structure ([26c87f2](https://gitlab.com/hestia-earth/hestia-schema/commit/26c87f2506069a2ff6172cd2f7cdf8bd012c7019))
* **yaml:** fix typos and add type ([889df81](https://gitlab.com/hestia-earth/hestia-schema/commit/889df81dc02f7bbb3904baf0e0f0e15f1552d870))


### Bug Fixes

* fix generators ([902630c](https://gitlab.com/hestia-earth/hestia-schema/commit/902630cfef7d208993f0dbda786d07d20f148db4))
* fix links and json-schema content ([3710b9e](https://gitlab.com/hestia-earth/hestia-schema/commit/3710b9e4f22d0ffa1a68a0115ec484534dee619a))
* **context:** handle not node types ([70322b3](https://gitlab.com/hestia-earth/hestia-schema/commit/70322b3b151f178b0fb9230767f292be20b527f9))
* **context:** update geojson context ([7d92dad](https://gitlab.com/hestia-earth/hestia-schema/commit/7d92dad5f1a35d04fce2ff11e06eb5f3e20a3291))
* **cycle:** remove identical extra example ([dfdc1b7](https://gitlab.com/hestia-earth/hestia-schema/commit/dfdc1b7d4d3e2a3f6ad01e91fc34736bd491f25d))
* **dist:** add missing dist files ([b1f91f7](https://gitlab.com/hestia-earth/hestia-schema/commit/b1f91f7bcbf228e0db54030516de9975ae77609d))
* **examples:** fix term values ([4d500da](https://gitlab.com/hestia-earth/hestia-schema/commit/4d500da90597867fce465f27077a37817004e499))
* **generate:** add missing name on JSONLD content ([ab0188f](https://gitlab.com/hestia-earth/hestia-schema/commit/ab0188f780e1c56ad8618a7d81bce3d75812f4e4))
* **generate:** merge utils into generate-types ([7987557](https://gitlab.com/hestia-earth/hestia-schema/commit/7987557725a4ae5f3792d5ccc6d6ccd8b49899c0))
* **generate types:** exclude class from own imports ([3fd28d6](https://gitlab.com/hestia-earth/hestia-schema/commit/3fd28d640dcb5c7939de9f3d156d233a948dffd5))
* **json-schema:** add missing type on $ref ([a30708a](https://gitlab.com/hestia-earth/hestia-schema/commit/a30708aac1c45ee062e11cdbd584b0e71012b591))
* **json-schema:** add missing type on array items ([bbbfaca](https://gitlab.com/hestia-earth/hestia-schema/commit/bbbfaca46812ae5135ab7565e3c202c5c697c93a))
* **json-schema:** clean up description content ([248acb5](https://gitlab.com/hestia-earth/hestia-schema/commit/248acb5511b1a0a11308eea3ba17f66eef518cce))
* **json-schema:** generate schema for JSON-Node ([ec399fb](https://gitlab.com/hestia-earth/hestia-schema/commit/ec399fb415885386d60b84865a35cd29d2bd4776))
* **json-schema:** remove type object on $ref ([965d950](https://gitlab.com/hestia-earth/hestia-schema/commit/965d950a76b87a4edc1a702fd696f1ca53b592af))
* **json-schema:** skip id property for non-node ([db89ad4](https://gitlab.com/hestia-earth/hestia-schema/commit/db89ad42fb8d4ba07b66ff77c0c817b972b7adcf))
* **json-schema:** use ajv for schema validation ([f223005](https://gitlab.com/hestia-earth/hestia-schema/commit/f223005922eec557f45a76058c8ee969143f1b3a))
* **schema:** set JSONLD as extension of NodeType ([2db652c](https://gitlab.com/hestia-earth/hestia-schema/commit/2db652ce5da89e105214fe613e72aee361bd194e))
* **scripts:** fix generate models form yaml ([5bdc399](https://gitlab.com/hestia-earth/hestia-schema/commit/5bdc3991e197870bd7cb7a05a75c0bceec9f7dec))
* **scripts:** fix table layout ([6a34900](https://gitlab.com/hestia-earth/hestia-schema/commit/6a34900c64ca9eaca44750e4345505dd1457a4d3))
* **scripts:** handle geojson type link ([9ba13d5](https://gitlab.com/hestia-earth/hestia-schema/commit/9ba13d5bfa0e6ee368aa75d1c39064275f4fd2cb))
* **scripts:** run scripts from top folder ([eaf9dfb](https://gitlab.com/hestia-earth/hestia-schema/commit/eaf9dfb5a7d88715971a2f81c2a4c6b512a58934))
* **template:** enable json-ld views for Nodes only ([f858d49](https://gitlab.com/hestia-earth/hestia-schema/commit/f858d49e36abeb840f6de9b664c4c801c39b3346))
* **templates:** fix context for jsonld ([35ee6d8](https://gitlab.com/hestia-earth/hestia-schema/commit/35ee6d87fb77729973a202c96d928b4e8a3d181d))
* **templates:** fix link to glossary ([8e11745](https://gitlab.com/hestia-earth/hestia-schema/commit/8e1174505c5e6b271c32d4543f41ebb026d3a258))
* **templates:** fix table width ([c6911f4](https://gitlab.com/hestia-earth/hestia-schema/commit/c6911f4cd59384db630de2ab4e2d25c9bc0e7faa))
* **templates:** fix wrong closing tag and update colors ([66698bf](https://gitlab.com/hestia-earth/hestia-schema/commit/66698bf6c4ddebd01e59ca22fc75a7fdc1984351))
* **templates:** reduce size 2nd level in menu ([a9f704e](https://gitlab.com/hestia-earth/hestia-schema/commit/a9f704ea8075a835c693e1ed255f51f4914e5042))
* **templates:** remove unused enum ([7abd0bf](https://gitlab.com/hestia-earth/hestia-schema/commit/7abd0bfc3a97a9aff3cd8f1fca2d3fd79861238e))
* **templates:** update styles ([390f289](https://gitlab.com/hestia-earth/hestia-schema/commit/390f2899a1d465f980a7ee7d2e1a366b3f99d99e))
* **templates:** use examples as array ([515e14a](https://gitlab.com/hestia-earth/hestia-schema/commit/515e14a4c632367a350f673531967965cf2e123e))
