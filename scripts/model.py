import glob
import re

import yaml


class Model:
    def __init__(self):
        self.types = []

    @staticmethod
    def load_yaml(folder):
        """
        Loads a model from a directory which contains YAML files that describe
        the model.
        :param folder: The directory that contains the YAML files (extension
                       *.yaml)
        """
        m = Model()
        d = folder if folder.endswith(
            '/') or folder.endswith('\\') else folder + '/'
        files = glob.glob(d + "*.yaml")
        for file_path in files:
            with open(file_path, 'r') as f:
                yaml_model = yaml.load(f)
                if 'class' in yaml_model:
                    m.types.append(ClassType.load_yaml(yaml_model['class']))
                if 'enum' in yaml_model:
                    m.types.append(EnumType.load_yaml(yaml_model['enum']))
        return m

    def find_type(self, name: str):
        if name is None:
            return None
        elif name.startswith('Ref['):
            return self.find_type('Ref')
        elif name.startswith('Embed['):
            return self.find_type('Embed')
        for t in self.types:
            if t.name == name:
                return t
        return None

    def get_super_classes(self, clazz):
        classes = []
        c = self.find_type(clazz.super_class)
        while c is not None:
            classes.append(c)
            c = self.find_type(c.super_class)
        return classes


class ClassType:
    def __init__(self, name=None, super_class=None, doc=None):
        self.name = name
        self.super_class = super_class
        self.doc = format_doc(doc)
        self.examples = []
        self.properties = []

    @staticmethod
    def load_yaml(yaml_model):
        c = ClassType()
        c.name = format_name(yaml_model['name'])
        c.type = yaml_model['type']
        if 'superClass' in yaml_model:
            c.super_class = yaml_model['superClass']
        c.doc = format_doc(yaml_model['doc']) if 'doc' in yaml_model else ''
        c.properties = list(filter(
            lambda p: not p.hidden, map(Property.load_yaml, yaml_model['properties']))
        ) if 'properties' in yaml_model else []
        c.examples = yaml_model['examples'] if 'examples' in yaml_model else []
        return c


class Property:
    def __init__(self, name=None, field_type=None, doc=None):
        self.name = name
        self.field_type = field_type
        self.display_type = field_type
        self.doc = format_doc(doc)
        self.default = None
        self.required = False
        self.hidden = False
        self.internal = False
        self.deprecated = False
        self.enum = []

    @staticmethod
    def load_yaml(yaml_model):
        p = Property()
        p.name = yaml_model['name']
        p.field_type = yaml_model['type']
        p.display_type = clean_field_type(yaml_model['type'])
        p.default = yaml_model['default'] if 'default' in yaml_model else None
        p.required = yaml_model['required'] if 'required' in yaml_model else False
        p.hidden = yaml_model['hidden'] if 'hidden' in yaml_model else False
        p.deprecated = yaml_model['deprecated'] if 'deprecated' in yaml_model else False
        p.internal = yaml_model['internal'] if 'internal' in yaml_model else False
        p.internal = True if p.deprecated else p.internal
        p.enum = yaml_model['enum'] if 'enum' in yaml_model else []
        p.doc = format_doc(yaml_model['doc'], p.enum, p.default) if 'doc' in yaml_model else ''
        return p

    @property
    def is_array(self):
      t = self.field_type
      return re.match(r'List\[[A-Z]+', t)

    @property
    def is_object(self):
      t = self.field_type
      return t.startswith('Embed[') or t.startswith('Ref[')

    @property
    def ref_class(self):
      t = self.field_type
      if t.startswith('List[Ref['):
          end = len(t) - 2
          return t[9:end]
      elif t.startswith('List['):
          end = len(t) - 1
          return t[5:end]
      elif t.startswith('Embed['):
          end = len(t) - 1
          return t[6:end]
      elif t.startswith('Ref['):
          end = len(t) - 1
          return t[4:end]
      return None

    @property
    def html_type_link(self):
        t = self.field_type
        if t.startswith('List[Ref['):
            end = len(t) - 2
            t = t[9:end]
        elif t.startswith('List['):
            end = len(t) - 1
            t = t[5:end]
        elif t.startswith('Embed['):
            end = len(t) - 1
            t = t[6:end]
        elif t.startswith('Ref['):
            end = len(t) - 1
            t = t[4:end]

        if t == 'GeoJSON':
            return "https://tools.ietf.org/html/rfc7946"
        elif t[0].isupper():
            return "./%s" % t
        elif t == 'iri':
            return "https://tools.ietf.org/html/rfc3987"
        elif t == 'date':
            return "https://json-schema.org/understanding-json-schema/reference/string.html#dates-and-times"
        elif t == 'date-time':
            return "https://json-schema.org/understanding-json-schema/reference/string.html#dates-and-times"
        elif t.startswith('array'):
            return "https://www.w3.org/2019/wot/json-schema#arrayschema"
        else:
            return "https://www.w3.org/2019/wot/json-schema#%sschema" % t


class EnumType:
    def __init__(self, name=None, doc=None):
        self.name = name
        self.doc = format_doc(doc)
        self.items = []

    @staticmethod
    def load_yaml(yaml_model):
        e = EnumType()
        e.name = yaml_model.get('name')
        e.doc = format_doc(yaml_model.get('doc')) if 'doc' in yaml_model else ''
        if 'items' in yaml_model:
            for item in yaml_model['items']:
                elem = EnumItem(item['name'])
                if 'doc' in item:
                    elem.doc = format_doc(item['doc'])
                e.items.append(elem)
        return e


class EnumItem:
    def __init__(self, name=None):
        self.name = name
        self.doc = ''


def clean_field_type(value) -> str:
    def ref_replace(match): return match.group(2)
    return re.sub('(Embed\[|Ref\[)([a-zA-Z]*)(\])', ref_replace, value)


def format_code_value(value) -> str:
    return '<code class="inline">' + str(value) + '</code>'


def format_doc(doc: str, enum=[], default=None) -> str:
    enums = f"<br/>Possible values are: {','.join(list(map(format_code_value, enum)))}" if len(enum) > 0 else ''
    default = f"<br/>Default to: {format_code_value(default)}" if default is not None else ''
    if doc is None:
        return ''
    for match in re.findall('\\[[^\\]]*\\]' '\(.+?\)', doc):
        part_1 = re.findall('\\[[^\\]]*\\]', match)
        part_1 = part_1[0][1:(len(part_1[0])-1)]
        part_2 = re.findall('\(.+?\)', match)
        part_2 = part_2[0][1:(len(part_2[0])-1)]
        link = '<a href="%s">%s</a>' % (part_2, part_1)
        doc = doc.replace(match, link)
    for match in re.findall('\\[[^\\]]*\\]', doc):
        part_1 = match[1:(len(match) - 1)]
        link = '<a href="./%s">%s</a>' % (part_1, part_1)
        doc = doc.replace(match, link)
    return f"{doc}{enums}{default}"


def format_name(name: str) -> str:
    return re.sub(r'(\w)([A-Z])', r'\1 \2', name)
