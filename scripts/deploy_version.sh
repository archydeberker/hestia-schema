#!/bin/sh

BUCKET=$1
PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | sed -e 's/ *//')

cd ./html
# remove all .html extensions
find . -name '*.html' -type f | while read NAME ; do mv "${NAME}" "${NAME%.html}" ; done
# keep index.html
mv index index.html

aws s3 sync . s3://$BUCKET/schema --content-type 'text/html'

# deploy also to version to swtich between versions
aws s3 sync . s3://$BUCKET/schema/$PACKAGE_VERSION --content-type 'text/html'

cd ..

# deploy assets to version as well
aws s3 sync ./assets s3://$BUCKET/schema/$PACKAGE_VERSION/assets

# deploy json-schema to version as well
aws s3 sync ./src/@hestia-earth/json-schema/json-schema s3://$BUCKET/schema/$PACKAGE_VERSION/json-schema --content-type 'application/json'
