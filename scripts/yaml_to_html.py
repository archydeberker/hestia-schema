import os
import model
from jinja2 import Environment, FileSystemLoader


def mkdirs(dest: str):
    if not os.path.isdir(dest):
        os.makedirs(dest)


mkdirs('./html')
with open('./versions.txt', 'r') as f:
    versions = f.read()
versions = ['Latest'] + list(filter(len, versions.split('\n')))[1:6]


def main():
    env = Environment(loader=FileSystemLoader('./scripts/templates'))
    index_template = env.get_template('index_template.html')
    class_template = env.get_template('class_template.html')
    m = model.Model.load_yaml('./yaml')
    write_index(m, index_template)
    for t in m.types:
        if type(t) == model.ClassType:
            write_class(class_template, m, t)


def write_class(template, m, t):
    super_classes = m.get_super_classes(t)
    examples = get_example(t.examples)
    text = template.render(model=t, super_classes=super_classes, examples=examples, versions=versions)
    file_path = './html/%s.html' % t.name.replace(' ', '')
    with open(file_path, 'w') as f:
        f.write(text)


def get_example(examples):
    def read_example(example):
        path = './examples/' + example
        with open(path, 'r') as f:
            example = f.read()
        return example
    return list(map(read_example, examples))


def write_index(m, template):
    concepts = []
    for t in m.types:
        concepts.append(t.name)
    text = template.render(concepts=concepts)
    with open('./index.html', 'w') as f:
        f.write(text)


if __name__ == '__main__':
    main()
