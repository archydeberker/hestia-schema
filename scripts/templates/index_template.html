<!DOCTYPE html>
<html>

{% include 'head.html' %}

<body class="has-spaced-navbar-fixed-top">
  {% include 'navbar.html' %}

  <section class="container py-2 mx-3 is-mx-desktop-auto mb-5">
    <div class="columns is-desktop">
      <div class="column navigation-menu is-narrow">
        <aside class="menu sticky">
          <p class="menu-label"></p>
          <ul class="menu-list">
            <li><a href="#background">Background</a></li>
            <li><a href="#introduction">Introduction</a></li>
            <li><a href="#flow">Flow of data</a></li>
            <li><a href="#file-type">File type</a></li>
            <li>
              <a class="menu-list">Overviews</a>
              <ul>
                <li><a class="is-size-7" href="#schema">Schema</a></li>
                <li><a class="is-size-7" href="#glossary">Glossary of Terms</a></li>
                <li><a class="is-size-7" href="#gap-fill-engine">Gap Filling Engine</a></li>
                <li><a class="is-size-7" href="#calc-engine">Calculation Engine</a></li>
                <li><a class="is-size-7" href="#api">API</a></li>
              </ul>
            </li>
          </ul>
        </aside>
      </div>

      <div class="column with-title-anchor">
        <h1 class="content mt-3 has-text-weight-bold is-size-3">About</h1>

        <hr class="section-divider my-4">

        <div id="background" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#background">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Background</span>
        </h2>

        <p class="content mb-3">
          Currently, information on the sustainability of foods is typically incomparable, unavailable,
          or unknown. There is often very little up-to-date, reliable, or practical information on the
          sustainability of foods produced using different practices or in different geographies. Without
          this information, the foundations for creating sustainable food supply-chains are missing.
        </p>

        <hr class="section-divider my-4">

        <div id="introduction" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#introduction">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Introduction</span>
        </h2>

        <p class="content mb-3">
          Hestia - Harmonized environmental storage and tracking of the impacts of agriculture - is an open-access
          platform that allows researchers and producers across the agri-food supply-chain to store data on the
          productivity and sustainability of agricultural products and production practices.
        </p>
        <p class="content mb-3">
          The data are then available to researchers, supporting scientific progress; to producers, helping
          them transition to more sustainable and productive practices; and to consumers, supporting better
          information around their choices and more sustainable consumption.
        </p>

        <hr class="section-divider my-4">

        <div id="flow" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#flow">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Flow of data</span>
        </h2>

        <section>
          <ol type="1" class="content pl-4">
            <li>
              <span class="content has-text-weight-bold">Data Sources:</span> Virtually any data describing specific
              products or producers can be uploaded, from field trial data to full supply-chain Life Cycle
              Assessments.
            </li>
            <li>
              <span class="content has-text-weight-bold"> Upload:</span> A researcher or producer uploads via the
              <a href="/files">data upload interface</a>.
            </li>
            <li>
              <span class="content has-text-weight-bold"> Validation:</span> The data are checked for consistency with
              the <a href="./Term">Schema</a> (a definition of the Hestia data structure); and checked for errors.
            </li>
            <li>
              <span class="content has-text-weight-bold"> Archiving:</span> Validated data are indexed
              and issued with a unique URL.
            </li>
            <li>
              <span class="content has-text-weight-bold">Calculation:</span>
              It is often possible to calculate further parameters of interest that were not provided in the original upload (for example the greenhouse gas emissions related to fertilizer manufacture).
              This happens in two stages.
              <ul class="content mt-0">
                <li>
                  Gap filling populates fields using geo-spatial data, global and regional default factors, and other sources.
                </li>
                <li>
                  Re-calculation uses a set of default models to estimate emissions and other environmentally relevant parameters.
                  This aims to make different datasets comparable.
                </li>
              </ul>
            </li>
            <li>
              <span class="content has-text-weight-bold">Aggregation:</span> The input data are a sample of individual
              products and producers globally. Each observation is weighted by its relative contribution to global agri-food
              production. Averages are then calculated for different products, geographies, years, and production systems.
            </li>
            <li>
              <span class="content has-text-weight-bold">Sharing:</span> The data are made available via the Hestia
              website and an API. We provide a <a href="https://pypi.org/project/hestia-earth.utils/">utilities library</a>
              which makes it easier to access and use Hestia data in Python.
            </li>
          </ol>
          <p class="content mb-4 has-text-weight-bold">
            Graphical overview of data flow
          </p>
          <img src="./assets/images/data_flow_overview.svg" max-width="800px" height="auto">
        </section>

        <hr class="section-divider my-4">

        <div id="file-type" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#file-type">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>File type</span>
        </h2>

        <p class="content mb-3">
          Data are stored in JSON Linked Data (<a href="https://www.w3.org/TR/json-ld11">JSON-LD</a>) files. This
          file type has two major benefits. Firstly, each data field links to a specific page on this website where
          it is defined and described. Secondly, each file contains hyperlinks to other JSON-LD files, enabling
          linkages between data, reuse of data, and minimisation of duplication in the database.
        </p>

        <hr class="section-divider my-4">

        <div id="upload" class="anchor"></div>

        <div id="schema" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#schema">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Overview: Schema</span>
        </h2>

        <section>
          <p class="content mb-3">
            Data for production systems are stored in multiple files which are unique and can often be reused. For
            example, data on soil pH is stored in a file describing a <a href="./Site">Site</a>, while data for each
            production <a href="./Cycle">Cycle</a> that occurs on the Site are stored in separate files (such as
            information on fertilizer applicaton and crop yield), allowing for multiple production Cycles on the same
            Site.
          </p>
          <p class="content mb-3">
            A data <a href="./Source">Source</a> is created when an <a href="./Actor">Actor</a> uploads data. It
            includes information about the upload, information about the study design, and full <a
              href="./Bibliography">Bibliography</a>.
          </p>
          <p class="content mb-3">
            Data describing production systems are stored at three levels:
          </p>
          <ul style="list-style: disc" class="content pl-4">
            <li>
              An <a href=" ./Organisation">Organisation</a> contains meta-data describing the farm, food processor,
              food packaging producer, or retailer.
            </li>
            <li>
              A <a href="./Site">Site</a> contains location specific data, for example it can be a field, aquaculture
              pond, building, or area of natural vegetation. It contains a link to the Organisation, meta-data, a set of
              <a href="./Measurements">Measurements</a> describing that site, and information about any <a
                href="./Infrastructure">Infrastructure</a> on the site, such as a greenhouse or factory.
            </li>
            <li>
              A Production <a href="./Cycle">Cycle</a> contains data on the set of <a href="./Input">Inputs</a> used,
              and the <a href="./Product">Products</a> and <a href="./Emission">Emissions</a> created during production,
              as well as a link to the Site the Cycle occurred on. It also includes a data <a
                href="./Completeness">Completeness</a> assessment, and a list of production <a
                href="./Practice">Practices</a> used.
            </li>
          </ul>
          <p class="content mb-3">
            An <a href="./ImpactAssessment">Impact Assessment</a> reports the environmental impact
            <a href="./Indicator">Indicators</a> related to producing one kilogram of a Product from the Cycle.
          </p>
          <p class="content mb-3">
            <a href="./Term">>> Go to the Schema</a>
          </p>
          <p class="content mb-4 has-text-weight-bold">
            Graphical overview of the data structure
          </p>
          <img src="./assets/images/schema_overview.svg" max-width="800px" height="auto">
        </section>

        <hr class="section-divider my-4">

        <div id="glossary" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#glossary">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Overview: Glossary of terms</span>
        </h2>

        <p class="content mb-3">
          Describing data consistently with the same terms makes data exchange possible. Hestia maintains a
          <a href="/glossary/">Glossary of Terms</a> describing key metrics, parameters, and aspects of the agri-food
          system, such as urea fertilizer, carbon dioxide, wheat grain, soil pH, or a lysimeter. Only these Terms can
          be used to describe data in Hestia.
        </p>

        <p class="content mb-3">
          It is possible to <a href="https://gitlab.com/hestia-earth/hestia-glossary">propose new Terms</a>
          if the current list doesn't meet your needs.
        </p>

        <p class="content mb-3">
          Default <a href="./Property">Properties</a> can be added to Terms, such as the scientific name of a crop, or
          the nitrogen content of urea fertilizer.
        </p>

        <p class="content mb-0">
          <a href="/glossary/">>> Go to the Glossary of Terms</a>
        </p>

        <p class="content mb-3">
          <a href="./Term">>> Go to the schema entry for Term</a>
        </p>

        <hr class="section-divider my-4">

        <div id="gap-fill-engine" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#gap-fill-engine">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Overview: Gap Filling Engine</span>
        </h2>

        <p class="content mb-3">
          It is often possible to add more data to Sites than was provided at
          upload using geospatial datasets, such maps of precipitation or temperature.
        </p>

        <p class="content mb-3">
          It is also possible to add further Input and Product data to Cycles using default
          factors and formulas, for example estimating the amount of straw produced based
          on crop yield and crop type.
        </p>

        <p class="content mb-3">
          The Gap Filling Engine assesses the current data and adds further data to the
          Site and Cycles where possible. The Gap Filling Engine uses some non-distributable
          datasets, and is therefore currently private.
        </p>

        <hr class="section-divider my-4">

        <div id="calc-engine" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#calc-engine">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Overview: Calculation Engine</span>
        </h2>

        <p class="content mb-3">
          The calculation engine provides a suite of biogeochemical and ecological models and
          linked Life Cycle Assessment methods, that allows emissions and other impact indicators
          to be estimated from the data in the Site and Cycle. It is provided as a
          <a href="https://pypi.org/project/hestia-earth.calculation/">Python library</a>.
        </p>

        <hr class="section-divider my-4">

        <div id="api" class="anchor"></div>

        <h2 class="title is-5">
          <a class="title-anchor" href="#api">
            <img src="./assets/images/link-solid.svg">
          </a>
          <span>Overview: API</span>
        </h2>

        <p class="content mb-3">
          The Hestia API allows provides read and write capabilities, and is documented
          on <a href="https://api-staging.hestia.earth/swagger/">Swagger</a>.
        </p>
      </div>
    </div>
  </section>

  {% include 'scripts.html' %}
</body>

</html>
