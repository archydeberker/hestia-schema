import os
import model
from jinja2 import Environment, FileSystemLoader


types = [
  'Cycle',
  'ImpactAssessment',
  'Site',
  'Organisation',
  'Source'
]


def main():
    env = Environment(loader=FileSystemLoader('./scripts/templates'))
    template = env.get_template('generator.html')
    models = model.Model.load_yaml('./yaml')
    text = template.render(models=models, types=types)
    with open('./html/generator.html', 'w') as f:
        f.write(text)


if __name__ == '__main__':
    main()
